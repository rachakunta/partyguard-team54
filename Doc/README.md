README / Installation instructions: 

This is an iOS Mobile application for the safety of women to prevent accidents and harassments in fraternity houses. At a fraternity house, if a woman faces any problem, she can report that issue to the fraternity members or host. She can report about various problems like fights, someone is too drunk/sick, suspect behavior, and any kind of assaults. The fraternity members (guards and host) can view the reported issues and they will send the rescue team to the spot. In a case of extreme emergency, the user can send an emergency request with emergency button option. If the issue does not resolve in 15 minutes, request will be automatically redirected to university police. 

Configuration:

This is an iOS Mobile application, which supports for all kinds of iPhone like iPhone 6, iPhone 6s and iPhone 6 plus with OS version of minimum 9.0

Installation Instructions:

Step-1: Download Xcode from App Store into your Mac system. Launch the .dmg file you downloaded. Follow the setup wizard to install Xcode Studio. 

Step-2: Our project source code is available on the CD and also in Bit bucket repository, we have given you access to the repository. Clone the code to your system or copy the git link and open the xcode, you will be prompted with options like create a new project, open an existing project and checkout as an existing project.

Click on �Checkout as an existing project� and paste the copied git link. The project will download to your Xcode and open up automatically.

Step-3: Select the device you want to run on at the top left corner of Xcode and click on run icon to build and run the app. The app runs on the simulator and can use the app.

Step-4: To deploy the app on your mobile device. Connect your mobile device and and select your mobile device under simulator. Run the app, you will be asked to enter the developer credentials. Enter your credentials, app gets deployed in the mobile device and you can use it. 
