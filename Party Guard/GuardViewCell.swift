//
//  GuardViewCell.swift
//  Party Guard
//
//  Created by Kola,Harish on 9/27/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This class contains the guard image and color label to display in the grid view of party guard team view controller
class GuardViewCell: UICollectionViewCell {
    
  
    ///UIView variable to hold the view
    @IBOutlet weak var ColorV: UIView!
    
   
    /// Guard image view variable to hold guard images
    @IBOutlet weak var GuardImageIV: UIImageView!
    
    /*
     method responsible for adjusting label
     */
    func adjustingLabelView()
    {
        ColorV.layer.cornerRadius = self.ColorV.frame.width/2
        ColorV.layer.masksToBounds = true
    }
    
}
