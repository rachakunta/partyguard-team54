//
//  SettingsViewController.swift
//  Party Guard
//
//  Created by Pilli,Bhavana on 9/23/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the settings functionalities - changing password and know your app 
class SettingsViewController: UIViewController{

    /// change password button
    @IBOutlet weak var changePasswordBTN: UIButton!
    
    /// edit profile button
    @IBOutlet weak var editProfileBTN: UIButton!
    /// logout services variable
    var logoutClass:LogoutService!
    /// app delegate object
    var appdelegate:AppDelegate!
    /// image view variable
    var retrievedImg:UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Settings"
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        // Do any additional setup after loading the view.
        addingLogoutBTN()
        buttonLayout(changePasswordBTN)
        buttonLayout(editProfileBTN)
        //buttonLayout(rateOurAppBTN)
       
        
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        addingImageasRightBarButton()
    }
    /*
     function adds image as a right bar button
     */
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(retrievedImg, forState: UIControlState.Normal)
        //add function for button
        button.addTarget(self, action: "imageButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    /*
     adds logout button
     */
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }
    
    /*
     adds logout functionality
     */
    func Logout_Functionality()
    {
        
        callingLogoutService()
        
    }
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    /*
     calls logout service
     */
    func callingLogoutService()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SettingsViewController.LogoutResponse(_:)), name: "Data Delivered", object: nil)
        logoutClass = LogoutService()
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
        //        statusCode = logoutClass.statusCode1
        //        print("code is \(statusCode)")
    }
    /**
     This function will acts based on logout response
     - Parameter notification: confirms if the password is resot or no
     */
    func LogoutResponse(notification: NSNotification)
    {
        print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
            displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /** function for button layout
     - Parameter buttonName:UIButton
     */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
