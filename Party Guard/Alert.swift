//
//  Alert.swift
//  Party Guard
//
//  Created by Rachakunta,Naga Sri Harsha Yadav on 9/27/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import Foundation
/// This class holds all the alert details
class Alert{
    
    /// outlet for universityName textfield
    var universityName:String!
    /// outlet for universityID textfield
    var universityID:Int!
    /// outlet for basicUserID textfield
    var basicUserID:Int!
    /// outlet for basicUserLastName textfield
    var basicUserLastName:String!
    /// outlet for basicUserFirstName textfield
    var basicUserFirstName:String!
    /// outlet for imgUrl textfield
    var imgUrl:String!
    /// outlet for issueID textfield
    var issueID:Int!
    /// outlet for issueName textfield
    var issueName:String!
    /// outlet for issueLocation textfield
    var issueLocation:String!
    /// outlet for issueDate textfield
    var issueDate:String!
    /// outlet for comments textfield
    var comments:String!
    /// outlet for fraternityName textfield
    var fraternityName:String!
    
    
    /**
     This is a default constructor
     */
    init(){
        
    }
    
    /**
     This is a parameterized constructor
     - Parameter universityName university name
     - Parameter universityID university id
     - Parameter basicUserID basic user id
     - Parameter basicUserLastName basic user last name
     - Parameter basicUserFirstName basic user first name
     - Parameter imgUrl image url
     - Parameter issueID issue id
     - Parameter issueName issue name
     - Parameter issueLocation location of the issue raised
     - Parameter issueDate date of the issue
     - Parameter comments comments on the issue
     - Parameter fraternityName name of the fraternity
     */
    
    init(universityName:String, universityID:Int, basicUserID:Int, basicUserLastName:String, basicUserFirstName:String, imgUrl:String, issueID:Int, issueName:String,issueLocation:String,issueDate:String, comments:String, fraternityName:String){
        self.universityName = universityName
        self.universityID = universityID
        self.basicUserID = basicUserID
        self.basicUserLastName = basicUserLastName
        self.basicUserFirstName = basicUserFirstName
        self.imgUrl = imgUrl
        self.issueID = issueID
        self.issueName = issueName
        self.issueLocation = issueLocation
        self.issueDate = issueDate
        self.comments = comments
        self.fraternityName = fraternityName
       
    }
    
}