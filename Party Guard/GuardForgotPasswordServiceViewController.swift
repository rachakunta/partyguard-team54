//
//  GuardForgotPasswordServiceViewController.swift
//  Party Guard
//
//  Created by Butna,Prasanth on 11/15/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements forgot password functionality for guard user

class GuardForgotPasswordServiceViewController: UIViewController {
    /// variable to store app delegate class
    var appdelegate:AppDelegate!
    /// outlet for statusCode1 textfield
    var statusCode1:Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Forgot Password"
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        hidingresetPasswrdFields(true)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "forgotResponse:", name: "Data Delivered", object: nil)
        
        
        // Do any additional setup after loading the view.
    }
    
    /**
     This function is an action which performs the submit action for forgot password
     
     - Parameter sender: sender of object - button
     */
    @IBAction func submitAction(sender: AnyObject) {
        if emailId.text == "" || emailId.text == nil
        {
            //displayAlertController("Input Alert", message: "Please enter your emailId")
            emailId.attributedPlaceholder = NSAttributedString(string: "Enter Email Id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if emailId.text != ""
        {
            let emailid:String = emailId.text!
            let statusOfValidation:Bool = validateEmail(emailid)
            if statusOfValidation == true
            {
                let passwordurl = appdelegate.servicesDomainUrl+"api/ForgotPassword/ForgotPassword"
                callingForgotPasswdService(["Email":emailId.text!], url: passwordurl)
            }
            else
            {
                print("entered email is not correct")
                displayAlertController("Input Alert", message: "Please enter valid email")
            }
            
        }
        
        
        
    }
    
    
    /**
     This function validates the email whether in acceptable format or not
     
     - Parameter enteredEmail:String is a String i.e., email entered
     */
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(enteredEmail)
        
    }
    
    /// outlet for submitBTN button
    @IBOutlet weak var submitBTN: UIButton!
    /// outlet for confirmPassword button
    @IBOutlet weak var confirmPassword: UITextField!
    /// outlet for newPassword button
    @IBOutlet weak var newPassword: UITextField!
    /// outlet for emailId button
    @IBOutlet weak var emailId: UITextField!
    /// outlet for resetPasswordBTN button
    @IBOutlet weak var resetPasswordBTN: UIButton!
    
    
    
    /**
     This function validates whether the both entered passowrds are matches and if matched and compliant to all rules then it will process the reset password action
     
     - Parameter sender: sender of object - AnyObject
     */
    @IBAction func resetPasswordAction(sender: AnyObject) {
        
        if newPassword.text == "" || newPassword.text == nil
        {
            //displayAlertController("Input Alert", message: "Please enter your new password")
            newPassword.attributedPlaceholder = NSAttributedString(string: "Enter New Password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if confirmPassword.text == "" || confirmPassword.text == nil
        {
            //displayAlertController("Input Alert", message: "Please confirm your password")
            confirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm New Password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else
        {
            let password:String = newPassword.text!
            let confirmPass:String = confirmPassword.text!
            print("entered email id correct")
            
            let statusOfpasswords = checkPasswords(password, confirmPwd: confirmPass)
            if statusOfpasswords == true
            {
                let passwordurl = appdelegate.servicesDomainUrl+"api/Account/NewPasswordSet"
                print("password url is \(passwordurl)")
                callserviceForPasswordReset(["Email":emailId.text!,"NewPassword":newPassword.text!,"ConfirmPassword":confirmPassword.text!], url: passwordurl )
                NSNotificationCenter.defaultCenter().addObserver(self, selector: "resetResponse:", name: "Data Delivered", object: nil)
            }
            
        }
        
        
    }
    /**
     This function checks whether the entered passwords are same or not
     
     - Parameter password:String is a password of type String, confirmPwd:String is a confirmed password of type String
     */
    
    func checkPasswords(password:String, confirmPwd:String)->Bool
    {
        if password ==  confirmPwd
        {
            return true
        }
        else{
            return false
        }
        
    }
    
    
    /**
     This function is to hide the reset password field
     
     - Parameter value:Bool is a flag value to evaluate when to hide
     */
    func hidingresetPasswrdFields(value:Bool)
    {
        newPassword.hidden = value
        confirmPassword.hidden = value
        resetPasswordBTN.hidden = value
    }
    
    /**
     This function is to hide the reset forget password field
     
     */
    
    func hidingForgtPasswordFields()
    {
        emailId.hidden = true
        submitBTN.hidden = true
    }
    
    /**
     This function calls the forgot password service and parses the response to retrieve the access token of a login user
     - Parameter params: dictionary with parameter names as keys and values of emaild, password and grant type
     - Parameter url: url of login service url
     */
    
    func callingForgotPasswdService(params : Dictionary<String, String>,url:String)
    {
        //print(params.debugDescription)
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print("request is \(request)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
                
            }).resume()
            
        }
        catch
        {
            print("something is wrong")
            //callbackforRegister()
        }
        
    }
    
    /**
     This function calls the reset password service and parses the response to retrieve the access token of a login user
     - Parameter params: dictionary with parameter names as keys and values of emaild, password and grant type
     - Parameter url: url of login service url
     */
    
    func callserviceForPasswordReset(params : Dictionary<String, String>,url:String)
    {
        print(params.debugDescription)
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print("request is \(request)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
                
            }).resume()
            
        }
        catch
        {
            print("something is wrong")
            //callbackforRegister()
        }
        
    }
    
    /**
     This function is an action which takes the control to the back page
     
     - Parameter sender: sender of object - button
     */
    
    @IBAction func backToLoginAction(sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func forgotResponse(notification: NSNotification)
    {
        print("status code is \(statusCode1!)")
        if statusCode1! == 200
        {
            hidingForgtPasswordFields()
            
            hidingresetPasswrdFields(false)
            
        }
        else
        {
            displayAlertController("Email", message: "Entered email id is not valid, Please register your email id")
        }
    }
    
    /**
     This function used to display an alert of the status of reset password action
     - Parameter notification: notification from NSNotification
     
     
     */
    
    func resetResponse(notification: NSNotification)
    {
        print("status code is \(statusCode1!)")
        if statusCode1! == 200
        {
            print("status in if loop is \(statusCode1!)")
            displayAlertController1("Reset Alert", message: "Your password has been reset")
        }
        else
        {
            displayAlertController1("Reset Alert", message: "Some thing has gone wrong with the server")
        }
    }
    
    /**
     This function redirects the screen to login View controller
     - Parameter title:String this is the title of the alert box
     - Parameter message:String this is the message to display as an alert
     */
    func displayAlertController1(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    func displayAlertController(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in}))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
