//
//  ProfileOfGuardViewController1.swift
//  Party Guard
//
//  Created by Kola,Harish on 10/23/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the edit and view profile functionality of basic user
class ProfileOfGuardViewController1: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var appdelegate:AppDelegate!
     var logoutClass:LogoutService!
    var statusCode1:Int!
     var retrievedImg:UIImage!
     var userDetails = Dictionary<String,String>()
    override func viewDidLoad() {
        super.viewDidLoad()
      
       
        hidingTF(true)
        self.navigationItem.title = "Profile"
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        addingLogoutBTN()
        buttonLayout(editProfileBTN)
        
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //print("In profile view controller email id : "+appdelegate.userDetails.emailId)
        if appdelegate.accesstoken != nil
        {
            print("access token from app delegate is \(appdelegate.accesstoken)")
            gettingUserInfo(appdelegate.accesstoken)
             NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileOfGuardViewController1.userinfoRes(_:)), name: "Data Delivered", object: nil)
        }
       
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        
        UserIV.image = retrievedImg
        addingImageasRightBarButton()

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBOutlet weak var lNameTF: UITextField!
    @IBOutlet weak var mNameTF: UITextField!
    @IBOutlet weak var fnameTF: UITextField!
    @IBOutlet weak var lNameLBL: UILabel!
    @IBOutlet weak var mNameLBL: UILabel!
    
    @IBOutlet weak var fNameLBL: UILabel!
    
    
    @IBOutlet weak var emailAddLBL: UILabel!
    
    @IBOutlet weak var phNOTF: UITextField!
    
    @IBOutlet weak var emailAddTF: UITextField!
    
    @IBOutlet weak var phNoLBL: UILabel!
    
    
    @IBOutlet weak var universityTF: UITextField!
    @IBOutlet weak var universityLBL: UILabel!
    
    
    @IBOutlet weak var editProfileBTN: UIButton!
    
    
    @IBOutlet weak var UserIV: UIImageView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        self.navigationItem.title = "Profile"
        //addSlideMenuButton()
    }
    
    //adding image button as right bar button
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(retrievedImg, forState: UIControlState.Normal)
        //add function for button
        
        let tapGesture = UITapGestureRecognizer(target: self, action: "imageButtonPressed")
        button.addGestureRecognizer(tapGesture)
        //button.addTarget(self, action: Selector("imageButtonPressed"), forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func imageButtonPressed(sender: UITapGestureRecognizer){
        let imageView = sender.view as! UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = self.view.frame
        newImageView.backgroundColor = .blackColor()
        newImageView.contentMode = .ScaleAspectFit
        newImageView.userInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: Selector("dismissFullscreenImage:"))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
    
    func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }

    
    @IBAction func editProfileAction(sender: AnyObject) {
        
        print("printing edit profile actionp \(fnameTF.text!)")
        
        //editProfileBTN.addTarget(self, action: #selector(ProfileOfGuardViewController1.Save(_:)), forControlEvents: UIControlEvents.TouchUpInside)
         if editProfileBTN.titleLabel?.text == "Save"
         {
            print("In save")
            print(fnameTF.text!)
            if fnameTF.text == "" || fnameTF.text == nil
            {
                
                fnameTF.attributedPlaceholder = NSAttributedString(string: "Enter First Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if lNameTF.text == "" || lNameTF.text == nil
            {
              
                lNameTF.attributedPlaceholder = NSAttributedString(string: "Enter Last Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if emailAddTF.text == "" || emailAddTF.text == nil
            {
                
                emailAddTF.attributedPlaceholder = NSAttributedString(string: "Enter Eamil Id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if phNOTF.text == "" || phNOTF.text == nil
            {
                
                phNOTF.attributedPlaceholder = NSAttributedString(string: "Enter Phone number", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if universityTF.text == "" || universityTF.text == nil
            {
                
                universityTF.attributedPlaceholder = NSAttributedString(string: "Enter University ", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if emailAddTF.text != ""
            {
                
                let emailid:String = emailAddTF.text!
                let statusOfValidation:Bool = validateEmail(emailid)
                if statusOfValidation == true
                {
                    print("firstName in save action is \(fnameTF.text!)")
                    callingUpdateProfileService(["firstName":fnameTF.text!,"lastName":lNameTF.text!,"PhoneNumber":phNOTF.text!,"imgUrl":"http://helloimage.com/image1","age":"34"], url: appdelegate.servicesDomainUrl+"api/Account/UpdateProfile")
                    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileOfGuardViewController1.editProfileResponse(_:)), name: "Data Delivered", object: nil)
                }
                else
                {
                    
                    print("entered email is not correct")
                    displayAlertController("Input Alert", message: "Please enter valid email")
                }
            }
            
        }
        else
         {
            editProfileBTN.setTitle("Save", forState: UIControlState.Normal)

            hidingTF(false)
            self.editPopulation()
        }
        
    }
    
    
    func userinfoRes(notification:NSNotification)
    {
        showingDataForLabels()
    }
    
    //populating labels with the retreived data
    func showingDataForLabels()
    {
        //print(" lastname label is \(userDetails["LastName"]!)")
        lNameLBL.text = appdelegate.userDetails.lastName
        fNameLBL.text = appdelegate.userDetails.firstName
        mNameLBL.text = ""
        phNoLBL.text = appdelegate.userDetails.phoneNo
        emailAddLBL.text = appdelegate.userDetails.emailId
        universityLBL.text = appdelegate.userDetails.university
        
    }
    
    //hiding all textfields
    func hidingTF(value:Bool)
    {
        lNameTF.hidden = value
        fnameTF.hidden = value
        mNameTF.hidden = value
        emailAddTF.hidden = value
        phNOTF.hidden = value
        universityTF.hidden = value
        
    }

    //adjusting button layout
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }
    

    //adding logout button as bar button item
    
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }
    
    func Save(sender: AnyObject) {
        var flag:Bool = true
        print(fnameTF.text!)
        if fnameTF.text == "" || fnameTF.text == nil
        {
            flag = false
            fnameTF.attributedPlaceholder = NSAttributedString(string: "Enter First Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if lNameTF.text == "" || lNameTF.text == nil
        {
            flag = false
            lNameTF.attributedPlaceholder = NSAttributedString(string: "Enter Last Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if emailAddTF.text == "" || emailAddTF.text == nil
        {
            flag = false
            emailAddTF.attributedPlaceholder = NSAttributedString(string: "Enter Eamil Id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if phNOTF.text == "" || phNOTF.text == nil
        {
            flag = false
            phNOTF.attributedPlaceholder = NSAttributedString(string: "Enter Phone number", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if universityTF.text == "" || universityTF.text == nil
        {
            flag = false
            universityTF.attributedPlaceholder = NSAttributedString(string: "Enter University ", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if emailAddTF.text != ""
        {
            flag = false
            let emailid:String = emailAddTF.text!
            let statusOfValidation:Bool = validateEmail(emailid)
            if statusOfValidation == true
            {
             print("firstName in save action is \(fnameTF.text!)")
                callingUpdateProfileService(["firstName":fnameTF.text!,"lastName":lNameTF.text!,"PhoneNumber":phNOTF.text!,"imgUrl":"http://helloimage.com/image1","age":"34"], url: appdelegate.servicesDomainUrl+"api/Account/UpdateProfile")
                NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileOfGuardViewController1.editProfileResponse(_:)), name: "Data Delivered", object: nil)
            }
            else
            {
                flag = false
                print("entered email is not correct")
                displayAlertController("Input Alert", message: "Please enter valid email")
            }
        }
        
        
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(enteredEmail)
        
    }
    
    func displayAlertController(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            self.presentViewController(alert, animated: true, completion: nil)
        }))
        
        
    }
    
    func displayAlertController1(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("HomeVC") as! SelectFraternityViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func callingUpdateProfileService(params : Dictionary<String, String>, url : String)
    {
        print("parameters in calling update profile service \(params.debugDescription)")
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.setValue("bearer "+appdelegate.accesstoken, forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            //print("request is \(request)")
            //print("request body is \(request.debugDescription)")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                //print("Response: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                //print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
                
            }).resume()
            
        }
        catch
        {
            print("something is wrong")
         
        }

    }
    
    func gettingUserInfo(accessToken:String!)
    {
        //var userDetails1 = Dictionary<String,String>()
        //print("access token is \(accessToken)")
        let request = NSMutableURLRequest(URL: NSURL(string: appdelegate.servicesDomainUrl+"api/Account/UserInfo")!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        
        do
        {
            print("do loop")
            request.setValue("Bearer "+accessToken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                //print("Response: \(response)")
                //                let strData = NSString(data: data!, encoding: NSUTF8StringEncoding) as! AnyObject
                var strData:[String:AnyObject]
                do
                {
                    try strData = NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as AnyObject! as! [String : AnyObject]
                    //print("Body: \(strData)")
                    let size = strData.count
                    //print("size of response data \(size)")
                    if size != 0
                    {
                        //print("access token is \(strData["access_token"]!)")
                        self.appdelegate.userDetails.firstName = strData["FirstName"]! as? String
                        self.appdelegate.userDetails.emailId = strData["Email"]! as? String
                        self.appdelegate.userDetails.lastName = strData["LastName"]! as? String
                        self.appdelegate.userDetails.university = strData["University"]! as? String
                        self.appdelegate.userDetails.phoneNo = strData["PhoneNumber"]! as? String
                        print("phone no in access token method is \(self.appdelegate.userDetails.phoneNo)")
//                        self.editPopulation()
                       
                        
                        //print("userName is \(strData["Email"]!)")
                        
                    }
                    else
                    {
                        self.callbackForProfile()
                        //self.dataResponse = 1
                    }
                    
                    dispatch_async(dispatch_get_main_queue()){
                        NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                    }

                }
                catch
                {
                    print("soome wrong")
                }
                
                
            }).resume()
            
        }
        
    }
    
    //populating the textfields with data from service
    
    func editPopulation()
    {
        print("first name in edit population is \(appdelegate.userDetails.firstName)")
        lNameTF.text = appdelegate.userDetails.lastName
        fnameTF.text = appdelegate.userDetails.firstName
        mNameTF.text = ""
        phNOTF.text = appdelegate.userDetails.phoneNo
        emailAddTF.text = appdelegate.userDetails.emailId
        universityTF.text = appdelegate.userDetails.university
    }

    // implementing call back function for profile
    
    func callbackForProfile()
    {
        
        if let path = NSBundle.mainBundle().pathForResource("profile", ofType: "json")
        {
            if let jsonData = try? NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
            {
                if let jsonResult:NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary
                {
                    self.userDetails["Email"] = jsonResult["Email"] as? String
                    //print("email  is \(jsonResult["Email"]!)")
                    
                    self.userDetails["FirstName"] = jsonResult["FirstName"] as? String
                    //print(self.userDetails["FirstName"])
                    self.userDetails["LastName"] = jsonResult["LastName"] as? String
                    self.userDetails["University"] = jsonResult["University"] as! String
                    self.userDetails["PhoneNumber"] = jsonResult["PhoneNumber"] as! String
                    //var age = item["Age"] as! String
                    
                }
            }
        }
        
        
    }
    

    func Logout_Functionality()
    {
        
        callingLogoutService()
        
    }
    
    func callingLogoutService()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileOfGuardViewController1.LogoutResponse(_:)), name: "Data Delivered", object: nil)
        logoutClass = LogoutService()
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
        //        statusCode = logoutClass.statusCode1
        //        print("code is \(statusCode)")
    }
  
    
    func editProfileResponse(notification: NSNotification)
    {
        if statusCode1 == 200
        {
            displayAlertController("Success Alert", message: "Your details has been saved!!")
            hidingTF(true)
            appdelegate.userDetails.lastName = lNameTF.text!
            appdelegate.userDetails.firstName = fnameTF.text!
            appdelegate.userDetails.phoneNo = phNOTF.text!
            print("phone number is \(appdelegate.userDetails.phoneNo)")
            appdelegate.userDetails.university = universityTF.text!
            showingDataForLabels()
            editProfileBTN.setTitle("Edit", forState: UIControlState.Normal)
            editProfileBTN.addTarget(self, action: #selector(ProfileOfGuardViewController1.editProfileAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
        }
        else
        {
            displayAlertController("Profile Alert", message: "Please check your details")
        }
    }
    
    func LogoutResponse(notification: NSNotification)
    {
        print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
            displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }
    
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    //Read the image picked from UIImagePickerController
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        print("Image Selected")
        self.dismissViewControllerAnimated(true, completion: nil)
        UserIV.image = image
        let data = UIImagePNGRepresentation(image)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "myImageKey")
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    
    @IBAction func importImage(sender: UIButton) {
        let myActionSheet = UIAlertController(title: "Select image", message: "Where would you add images from?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        // choose from photos action button
        let chooseAction = UIAlertAction(title: "Choose from photos", style: UIAlertActionStyle.Default) { (action) in
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            image.allowsEditing = false
            self.presentViewController(image, animated: true, completion: nil)
        }
        //photo action button
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default) { (action) in
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.Camera
            image.allowsEditing = false
            self.presentViewController(image, animated: true, completion: nil)
        }
        
        // cancel action button
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) in
            print("Cancel action button tapped")
        }
        // add action buttons to action sheet
        myActionSheet.addAction(chooseAction)
        myActionSheet.addAction(takePhotoAction)
        myActionSheet.addAction(cancelAction)
        // support iPads (popover view)
        if let popoverController = myActionSheet.popoverPresentationController {
            popoverController.sourceView = sender as! UIView
            popoverController.sourceRect = sender.bounds
        }
        //myActionSheet.popoverPresentationController!.sourceView = self.
        //        myActionSheet.popoverPresentationController!.sourceRect = CGRectMake(self.addImageBTN.bounds.size.width / 2.0, self.addImageBTN.bounds.size.height / 2.0, 1.0, 1.0)
        
        //        myActionSheet.popoverPresentationController?.sourceView = self.view
        //        myActionSheet.popoverPresentationController?.sourceRect = self.view.bounds
        // present the action sheet
        self.presentViewController(myActionSheet, animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
