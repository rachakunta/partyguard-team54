//
//  UserRequestHistoryViewController.swift
//  Party Guard
//
//  Created by Harish K on 10/11/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit

/// This view controller implements the user request history functionality of basic user
class UserRequestHistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /// declaration of user requests
    var userRequests:[UserRequest] = []
    
    ///daclration of logout class object
     var logoutClass:LogoutService!
    
    ///declaration of app delegate object
    var appdelegate:AppDelegate!
    
    ///declaration of user request class object
    var userRequestClass = UserRequest()
    
    ///declaration of table view object
    var tblv = UITableView()
    
    ///declaration of retreived image
    var retrievedImg:UIImage!
    
    ///declaration of request 1
    var request1 = UserRequest(date: "Aug 21", incident: "Accident", fraternity: "Kappa Delta", location: "First Floor", comments: "Emergency", university: "NWMSU", status: "Revert")
    
    ///declaration of request 2
    var request2 = UserRequest(date: "Sep 04", incident: "Suspect", fraternity: "Sigma Phi", location: "Second Floor", comments: "Need Help", university: "NWMSU", status: "Resolved")
    override func viewDidLoad() {
        super.viewDidLoad()
        addingLogoutBTN()
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        userRequests.append(request2)
        userRequests.append(request2)
        
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        addingImageasRightBarButton()      
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        self.tblv.reloadData()
        print("Reloaded data")
    }
    
    
    /*
     This function adds the image to the right bar button
     
    */
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(UIImage(named: "ProfileIcon"), forState: UIControlState.Normal)
        //add function for button
        button.addTarget(self, action: "imageButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }

    override func viewDidAppear(animated: Bool) {
       
        self.navigationItem.title = "Requests"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     This function adds the logout button
     
     */
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("user requests count")
        return userRequests.count
    }
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        print("in userrequest view1")
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("userrequestcell")!
        self.tblv = tableView
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clearColor()
        print("in userrequest view")
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        let fraternityLBL : UILabel = cell.contentView.viewWithTag(101) as! UILabel!
        let dateLBL : UILabel = cell.contentView.viewWithTag(102) as! UILabel!
        let statusLBL : UILabel = cell.contentView.viewWithTag(103) as! UILabel!
        
        imgIcon.layer.borderWidth = 1.0
        imgIcon.layer.masksToBounds = false
        imgIcon.layer.borderColor = UIColor.blackColor().CGColor
        imgIcon.layer.cornerRadius = imgIcon.frame.size.height/2
        imgIcon.clipsToBounds = true
        
        //imgIcon.image = UIImage(named: fraternitiesList[indexPath.row].icon)
        fraternityLBL.text = userRequests[indexPath.row].fraternity
        dateLBL.text = userRequests[indexPath.row].date
        statusLBL.text = userRequests[indexPath.row].status
        return cell
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "requestToRequestHistory") {
            print("sample example \(self.tblv.indexPathForSelectedRow!.row))")
            
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destinationViewController as! UserRequestHistoryDetailViewController
            // your new view controller should have property that will store passed value
            //NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
            //print("Selected Row is \(self.tableView.indexPathForSelectedRow)")
            print("prepare for segue in userrequestVC")
            viewController.selectedRequest = self.userRequests[self.tblv.indexPathForSelectedRow!.row]
        }

    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func Logout_Functionality()
    {
        
        callingLogoutService()
        
    }
    /*
     This function calls the logout service
     
     */
    func callingLogoutService()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "LogoutResponse:", name: "Data Delivered", object: nil)
        logoutClass = LogoutService()
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
        //        statusCode = logoutClass.statusCode1
        //        print("code is \(statusCode)")
    }
    
    /*
     This function populates the university response
     
     */
    func populateUnivResponse(notification: NSNotification)
    {
        
    }
    
    /*
     This function displays the logout confirmation
     
     */
    func LogoutResponse(notification: NSNotification)
    {
        print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
            displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }
    
    /*
     This function displays the alert for logout
     
     */
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }


}
