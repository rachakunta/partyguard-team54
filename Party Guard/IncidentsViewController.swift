//
//  I/Users/s525097/Desktop/partyguard-ios-team54/Party Guard/LocationViewController.swiftncidentsViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 8/31/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controllr implements the functionality of selecting incidents from different incidents for basic user
class IncidentsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    /// fraternity image view variable
    @IBOutlet weak var fraternityIMG: UIImageView!
    /// fraternity lable
    @IBOutlet weak var fraternityLBL: UILabel!
    /// incidents array list
    var incidentsList:[String] = ["Accident", "Feeling Unsafe", "Fight", "Other"]
    /// selected fraternity object
    var selectedFraternity:Fraternity!
    
    //var passedValue:[String:[String]]!
    /// table view object
    var tblv:UITableView!
    
    override func viewDidLoad() {
        //fraternityIMG.image = UIImage(named: selectedFraternity.icon)
        fraternityLBL.text = selectedFraternity.fraternityName
//        let incidentTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel!
//        incidentTitle.text = 
        //incidentsList.removeAll()
//        for i in selectedFraternity.incidentsTypes.keys{
//            incidentsList.append(i)
//        }
        
        //print("in incidentsVC \(passedValue.count)")
        super.viewDidLoad()
        
        adjustingView()
        
        //code to remove unused cells of table view
        //self.tableView.tableFooterView = UIView(frame: CGRectZero)
        //self.tableView.tableFooterView?.hidden = true
        
        // Do any additional setup after loading the view.
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /**
     Method displays number of secion in table view
     - Parameter tableView: UITableView table view object
     - returns: number of sections
    */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        print("in incidents view controller")
        return 1
    }
    /**
     Method displays number of rows in section
     - Parameter tableView: UITableView table view object\
     - Parameter section : Int
     returns: number of rows in sections
     */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("in incidents view controller count \(incidentsList.count)")
        return incidentsList.count
    }
    /**
     Method displays cells
     - Parameter tableView: UITableView table view object\
     - Parameter indexPath: NSIndexPath
     returns: number UITableViewCell
     */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("incidentsCell")! as UITableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clearColor()
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel!
       
        
        tblv = tableView
       
        lblTitle.text = incidentsList[indexPath.row]
        //
        return cell
        
    }
    /**
     Method is a functin of emergency button
     - Parameter sender: AnyObject
     */
    @IBAction func emergencyButton(sender: AnyObject) {
        let alert = UIAlertController(title: "Are you sure you want to rise an Emergency request?", message: "", preferredStyle: .Alert)
//        let alert1 = UIAlertController(title: "Your request has been raised", message: "", preferredStyle: .Alert)
//        alert1.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in }))
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        //        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        //        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        //
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: .Default, handler:handleDone))
        
        
        
        self.presentViewController(alert, animated: true, completion: {
            

            print("completion block")
        })

        
    }
    
    var tField: UITextField!
    /**
     method generates text field in the alert box
     - Parameter textField: UITextField!
     */
    func configurationTextField(textField: UITextField!)
    {
        print("generating the TextField")
        textField.placeholder = "Comments"
        tField = textField
        
    }
    
    /**
     method handles done button functinoalities
     - Parameter alertView: UIAlertAction!
     */
    func handleDone(alertView: UIAlertAction!){
        let alert1 = UIAlertController(title: "Your request has been raised successfully!!", message: "", preferredStyle: .Alert)
        alert1.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in }))
        self.presentViewController(alert1, animated: true, completion: {
            
            
            print("completion block")
        })
    }
    
    /**
     method handles cancel button functinoalities
     - Parameter alertView: UIAlertAction!
     */
    func handleCancel(alertView: UIAlertAction!)
    {
        
        print("Cancelled !!")
        
    }
    /// displays alert
    func displayAlert()
    {
        print("Got it harsha")
      
        let alertController = UIAlertController(title: "iOScreator", message:
            "Hello, world!", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    /// fraternity image view variable
    @IBOutlet weak var fraternityImageIV: UIImageView!
    
    func adjustingView()
    {
        fraternityImageIV.layer.borderWidth = 1.0
        fraternityImageIV.layer.masksToBounds = false
        fraternityImageIV.layer.borderColor = UIColor.blackColor().CGColor
        fraternityImageIV.layer.cornerRadius = fraternityImageIV.frame.size.height/2
        fraternityImageIV.clipsToBounds = true
    }
    /// sends data from one view controller to another
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if (segue.identifier == "incidentToLocation") {
            
            
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destinationViewController as! LocationViewController
            // your new view controller should have property that will store passed value
            //NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
            //print("Selected Row is \(self.tableView.indexPathForSelectedRow)")
            print(incidentsList[1])
            //print("incidentvalue passed vliues are \(passedValue[incidentsList[self.tblv.indexPathForSelectedRow!.row]])")
            //viewController.location = incidentsList[self.tblv.indexPathForSelectedRow!.row]
            viewController.selectedFraternity = self.selectedFraternity
            viewController.incident = incidentsList[self.tblv.indexPathForSelectedRow!.row]
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
