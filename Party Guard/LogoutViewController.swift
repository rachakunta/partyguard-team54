//
//  LogoutViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 8/31/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the logout functionality -  dummy
class LogoutViewController: UIViewController {

    /// appdelegate object
    var appdelegate:AppDelegate!
    /// status code object
    var statusCode1:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if appdelegate.accesstoken != nil
        {
            CallinglogoutApi(appdelegate.accesstoken)
        }
        else
        {
            callbackforLogout()
        }
       
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "LogoutResponse:", name: "Data Delivered", object: nil)
        // Do any additional setup after loading the view.
    }
    /**
     this method calls logout API
     - Parameter accessToken:String Access token
 
    */
    func CallinglogoutApi(accessToken:String)
    {
        let request = NSMutableURLRequest(URL: NSURL(string: appdelegate.servicesDomainUrl+"api/Account/Logout")!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        do
        {
            print("do loop")
            request.setValue("Bearer "+accessToken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
            }).resume()
        }
        catch
        {
            print("something is wrong")
        }
        
    }
    
    /**
     This function alerts the user based on service response
     
     - Parameter  notification: NSNotification
     
     */
    
    func LogoutResponse(notification: NSNotification)
    {
        print("status code is \(statusCode1!)")
        if statusCode1 == 200
        {
            self.displayAlertController1("Logout alert", message: "Successful logout")
        }
        else
        {
            callbackforLogout()
        }
    }
    
    
    /**
     callback funtion for logout
     */
    func callbackforLogout()
    {
        displayAlertController1("Logout alert", message: "Successful logout")
    }
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertController1(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
  
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
