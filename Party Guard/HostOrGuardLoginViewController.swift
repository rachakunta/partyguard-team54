///Users/s525030/Desktop/partyguard-ios-team54/Party Guard/ProfileViewController.swift
//  GuardLoginViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 9/15/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the login functionality of host and guard user
class HostOrGuardLoginViewController: UIViewController {

    /// outlet for  email textfield
    @IBOutlet weak var emailTF: UITextField!
    /// access token of a user
    var access_token:String!
    /// outlet for password textfield
    @IBOutlet weak var passwordTf: UITextField!
    /// declaring variable for app delegate object
    var appdelegate:AppDelegate!
    
    /// declaring outlet for login button
    @IBOutlet weak var guardLoginBTN: UIButton!
    
    /// declaring variable to store error status code
     var errorStatus:Int!
    
    /// declaring and initialising dictionary to store user details
    var userDetails = Dictionary<String,String>()
    @IBOutlet weak var guardCreateAccountBTN: UIButton!
    var statusOfLogin:String!
    /// declaring variable for guardtabobject
    var GuardTabObject:GuardTabBarViewController!
    /// declaring variable for user class
    var userClass:UserDetails!
    
    
    /**
     This function is an action which validates the user details and calls login service
     - Parameter sender: sender of object - button
     */
    @IBAction func loginBTN(sender: AnyObject) {
        if(emailTF.text?.characters.count < 1){
            displayAlertController("Email/Username", message: "Please enter valid username")
        }
        else if(passwordTf.text?.characters.count < 1){
                displayAlertController("Password", message: "Password field should not be blank")
        }
        else
        {
            
            let url = appdelegate.servicesDomainUrl+"token"
            print("url is \(url)")
           post(["username":emailTF.text!,"password":passwordTf.text!,"grant_type":"password"],url: url)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HostOrGuardLoginViewController.loginResponse(_:)), name: "Data Delivered", object: nil)
//            if emailTF.text == "guard"
//            {
//                statusOfLogin = "guard"
//                //GuardTabObject = GuardTabBarViewController()
//                
//            }
//            else{
//                statusOfLogin = "host"
//            }
        }
    }
    
    /**
     This function calls the login service and parses the response to retrieve the access token of a login user
     - Parameter params: dictionary with parameter names as keys and values of emaild, password and grant type
     - Parameter url: url of login service url
     */
    func post(params : Dictionary<String, String>, url : String)
    {
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        var bodyData = ""
        for (key,value) in params{
            if (value == ""){ continue }
            let scapedKey = key.stringByAddingPercentEncodingWithAllowedCharacters(
                .URLHostAllowedCharacterSet())!
            let scapedValue = value.stringByAddingPercentEncodingWithAllowedCharacters(
                .URLHostAllowedCharacterSet())!
            bodyData += "\(scapedKey)=\(scapedValue)&"
            
        }
        let data1:String = bodyData.substringToIndex(bodyData.endIndex.predecessor())
        print("body data is   \(data1)")
        do
        {
            print("do loop")
            try request.HTTPBody = data1.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
            request.addValue("text/plain", forHTTPHeaderField: "Accept")
            print("request is \(request.debugDescription)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                //print("Response: \(response)")
                //                let strData = NSString(data: data!, encoding: NSUTF8StringEncoding) as! AnyObject
                //var normalResponse:NSURLResponse = nil
                if response != nil
                {
                    var strData:[String:AnyObject]!
                    do
                    {
                        try strData = NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as AnyObject! as! [String : AnyObject]
                        //print(strData.count)
                        //print("Body: \(strData)")
                        if strData["error_description"] != nil
                        {
                            self.errorStatus = 1
                        }
                        else
                        {
                            self.errorStatus = 0
                            //print("access token is \(strData["access_token"]!)")
                            self.access_token = strData["access_token"]! as! String
                            
                            
                            // calling user service class object and populating user details class with the retreived data from api
                            
                            
                            //pprint("user firstname from user class is \(self.appdelegate.userDetails.firstName!)")
                            print("userName is \(strData["userName"]!)")
                            self.appdelegate.accesstoken = self.access_token
                            self.gettingAccessToken(self.access_token)
                            
                            // calling user details api with the retrieved access token and url
                            
                        }
                        
                        dispatch_async(dispatch_get_main_queue()){
                            NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                        }
                        
                    }
                    catch
                    {
                        print("some wrong --  calling call back functions ")
                        self.callbackforLogin()
                    }
                }
                
                
                
                
                
            }).resume()
            
        }
        //task.resume()
    }
    
    
    func gettingAccessToken(accessTok:String)->String
    {
        //print("access is \(accessTok)")
        return accessTok
    }

    /// callback for login service of guard user
    func callbackforLogin()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserHomeVC") as! UserTabBarViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTF.clearButtonMode = UITextFieldViewMode.WhileEditing
        passwordTf.clearButtonMode = UITextFieldViewMode.WhileEditing
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
         self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        // Do any additional setup after loading the view.
        
        buttonLayout(guardLoginBTN)
        buttonLayout(guardCreateAccountBTN)
    }
    
    
    /**
     This function used to call after login response from service by using error status and redirecting to login screen based on response
     - Parameter notification: notification from NSNotification
     
     */
    func loginResponse(notification:NSNotification)
    {
        //appdelegate.accesstoken = gettingAccessToken()
        if errorStatus == 1
        {
            displayAlertController("Login Alert", message: "Incorrect Username/password")
            
        }
        else if errorStatus == 0
        {
            print("access tok is \(appdelegate.accesstoken)")
            callserviceforUserinfo()
            //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HostOrGuardLoginViewController.userInfoResponse(_:)), name: "Data Delivered", object: nil)
//            let guardTBC = GuardTabBarViewController()
//            guardTBC.hostOrGuard = "guard"
//            callserviceforUserinfo()
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserHomeVC") as! UserTabBarViewController
//            self.presentViewController(nextViewController, animated: true, completion: nil)
        }
     
    }
    
//    func userInfoResponse(notification:NSNotification)
//    {
//        
//
//    }

    /// This function calls the userinfo service and parses the response from service and store it in the userDetails class

    func callserviceforUserinfo()
    {
        
        var firstName:String!
        var lastName:String!
        var middleName:String!
        var emailId:String!
        var status:Int!
        var phoneNo:String!
        var userImage:String!
        var userType:String!
        var university:String!
        print("access token is \(appdelegate.accesstoken)")
        let request = NSMutableURLRequest(URL: NSURL(string: appdelegate.servicesDomainUrl+"api/Account/UserInfo")!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        
        do
        {
            print("do loop")
            request.setValue("Bearer "+appdelegate.accesstoken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                //                let strData = NSString(data: data!, encoding: NSUTF8StringEncoding) as! AnyObject
                var strData:[String:AnyObject]
                do
                {
                    try strData = NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as AnyObject! as! [String : AnyObject]
                    print("Body: \(strData)")
                    var size = strData.count
                    print("size of response data \(size)")
                    if size != 0
                    {
                        //print("access token is \(strData["access_token"]!)")
                        firstName = strData["FirstName"]! as? String
                        emailId = strData["Email"]! as? String
                        lastName = strData["LastName"]! as? String
                        status = strData["Status"]! as? Int
                        phoneNo = strData["PhoneNumber"]! as? String
                        userType = strData["UserType"]! as? String
                       
                        self.userClass = UserDetails(firstName: firstName, lastName: lastName, middileName: "", emailId: emailId, university: "None", phoneno: phoneNo,userType: userType,status:status)
                        self.appdelegate.userDetails = self.userClass
                                              //                        self.editPopulation()
                     
                        
                        print("userName is \(strData["Email"]!)")
                        
                    }
                    else
                    {
                        //self.callbackForProfile()
                        //self.dataResponse = 1
                    }
                    
                    dispatch_async(dispatch_get_main_queue()){
                       // NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                        if self.appdelegate.userDetails.userType == "guard"
                        {
                            // navigate to guard view
                            //statusOfLogin = "guard"
                            print(" user type is \(self.appdelegate.userDetails.userType)")
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("GuardTabBar") as! GuardTabBarViewController
                            self.presentViewController(nextViewController, animated: true, completion: nil)
                            
                        }
                        else if self.appdelegate.userDetails.userType == "host"
                        {
                            // navigate to host view
                            //statusOfLogin = "host"
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            
                            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("GuardTabBar") as! GuardTabBarViewController
                            self.presentViewController(nextViewController, animated: true, completion: nil)
                            
                        }

                    }
                    
                }
                catch
                {
                    print("soome wrong")
                }
                
                
            }).resume()
            
        }

    }
    
    /**
     This function adjusts the button edges to round corner
     - Parameter buttonName: button to be adjusted
     */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /**
     This function displays alerts to user with title and appropriate message
     - Parameter title: title of alert
     - Parameter message: description of alert
     
     */
    
    func displayAlertController(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in}))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
