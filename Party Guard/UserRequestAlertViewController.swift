//
//  UserRequestAlertViewController.swift
//  Party Guard
//
//  Created by Rachakunta,Naga Sri Harsha Yadav on 9/15/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller used to view the user request alerts
class UserRequestAlertViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //displayAlert()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    /**
     method responsible for click me button
     - Parameter sender: AnyObject
     */
    @IBAction func clickME(sender: AnyObject) {
        let alert = UIAlertController(title: "Are you sure?", message: "--------------------------", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
//        alert.addTextFieldWithConfigurationHandler(configurationTextField)
//        alert.addTextFieldWithConfigurationHandler(configurationTextField)
//        
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in
            print("Done !!")
            
            print("Item : \(self.tField.text)")
        }))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })

        
    }
    
    /// text field variable
    var tField: UITextField!
    
    /**
     method generates text field in the alert box
     - Parameter textField: UITextField!
     */
    
    func configurationTextField(textField: UITextField!)
    {
        print("generating the TextField")
        textField.placeholder = "Comments"
        tField = textField
    }
    
    /**
     method responsible for cancel button
     - Parameter textField: UITextField!
     */
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    /// responsible for dislaying an alert
    func displayAlert()
    {
    print("Got it harsha")
//    let alert = UIAlertController(title: "Enter Input", message: "", preferredStyle: .Alert)
//    
//    alert.addTextFieldWithConfigurationHandler(configurationTextField)
//    alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:handleCancel))
//    alert.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in
//    print("Done !!")
//    
//    print("Item : \(self.tField.text)")
//    }))
//    self.presentViewController(alert, animated: true, completion: {
//    print("completion block")
//    })
//        print("harsha yadav")
        let alertController = UIAlertController(title: "iOScreator", message:
            "Hello, world!", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
