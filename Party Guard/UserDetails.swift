//
//  UserDetails.swift
//  Party Guard
//
//  Created by Harish K on 10/29/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import Foundation
/// This class contains the initializer and variables to hold user details
class UserDetails
{
    /// outlet for firstName textfield
    var firstName:String!
    /// outlet for lastName textfield
    var lastName:String!
    /// outlet for middleName textfield
    var middleName:String!
    /// outlet for emailId textfield
    var emailId:String!
    /// outlet for university textfield
    var university:String!
    /// outlet for phoneNo textfield
    var phoneNo:String!
    /// outlet for userImage textfield
    var userImage:String!
    /// outlet for userType textfield
    var userType:String!
    /// outlet for status textfield
    var status:Int!
    
    /**
     This is a default constructor
     */
    init(){
        
    }
   
    
    /**
     This is a parameterized constructor
     - Parameter firstName first name
     - Parameter lastName last name
     - Parameter middileName middle name
     - Parameter emailId email id
     - Parameter university university name
     - Parameter phoneno phone number
     - Parameter userType type of the user
     - Parameter status status details
     
     */
    init(firstName:String,lastName:String,middileName:String,emailId:String,university:String,phoneno:String,userType:String,status:Int)
    {
        self.firstName = firstName
        self.lastName = lastName
        self.middleName = middileName
        self.emailId = emailId
        self.university = university
        self.phoneNo = phoneno
        self.userType = userType
        self.status = status
    }
    
    
    
    
}
