//
//  UserRequest.swift
//  Party Guard
//
//  Created by Harish K on 10/12/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import Foundation
/// This class holds the details of user request
class UserRequest{
    /// outlet for date textfield
    var date:String!
    /// outlet for incident textfield
    var incident:String!
    /// outlet for fraternity textfield
    var fraternity:String!
    /// outlet for location textfield
    var location:String!
    /// outlet for comments textfield
    var comments:String!
    /// outlet for university textfield
    var university:String!
    /// outlet for status textfield
    var status:String!
    
    /**
     This is a default constructor
     */
    init(){
        
    }
    
    
    
    /**
     This is a parameterized constructor
     - Parameter date  date of user request
     - Parameter incident user request incident
     - Parameter fraternity fraternity where user requested
     - Parameter location location where user requested
     - Parameter comments comments user presented
     - Parameter university university where user raised request
     - Parameter status status of the request
    
     */
    init(date:String, incident:String, fraternity:String, location:String, comments:String, university:String, status:String){

        self.date = date
        self.incident = incident
        self.fraternity = fraternity
        self.location = location
        self.comments = comments
        self.university = university
        self.status = status
     
        
    }
}