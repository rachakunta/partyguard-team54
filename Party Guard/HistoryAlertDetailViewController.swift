//
//  ViewController.swift
//  Party Guard
//
//  Created by Harish K on 10/9/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the each alert history detail of guard user/host user
class HistoryAlertDetailViewController: UIViewController {
    /// variable to hold alet history
    var historyDetails = HostAlertHistory()
    /// user image view variable
    @IBOutlet weak var userIMG: UIImageView!
    /// username label
    @IBOutlet weak var userName: UILabel!
    /// fraternity name label
    @IBOutlet weak var fraternityName: UILabel!
    /// alert type label
    @IBOutlet weak var alertType: UILabel!
    /// location lable
    @IBOutlet weak var location: UILabel!
    /// comments lable
    @IBOutlet weak var comments: UILabel!
    ///status label
    @IBOutlet weak var status: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        //userIMG.
        userName.text = historyDetails.basicUserfirstname + historyDetails.basicUserfirstname
        fraternityName.text = historyDetails.fraternityName
        alertType.text = historyDetails.issueName
        location.text = historyDetails.issueLocation
        comments.text = historyDetails.comments
        status.text = "Resolved"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
