//
//  File.swift
//  Party Guard
//
//  Created by Rachakunta,Naga Sri Harsha Yadav on 9/29/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import Foundation
/// This class is used to store the details fraternity like name, id, code, payment status, university name
class Fraternity{
    /// outlet for fraternityName textfield
    var fraternityName:String!
    /// outlet for fraternityID textfield
    var fraternityID:Int!
    /// outlet for fraternitynickName textfield
    var fraternitynickName:String!
    /// outlet for subscripCode textfield
    var subscripCode:String!
    /// outlet for paymentStatus textfield
    var paymentStatus:String!
    /// outlet for chapter textfield
    var chapter:String!
    /// outlet for tempPass textfield
    var tempPass:String!
    /// outlet for univeristyName textfield
    var univeristyName:String!
    /// outlet for universityId textfield
    var universityId:Int!
    /// outlet for locationid textfield
    var locationid:Int!
    /// outlet for guardCode textfield
    var guardCode:String!
    
    /**
     This is a parameterized constructor
     - Parameter fraternityName name of the fratenity
     - Parameter fraternityID fraternity ID
     - Parameter fraternitynickName fraternity nick name
     - Parameter subscripCode subscription code
     - Parameter paymentStatus payment status
     - Parameter chapter chapter name
     - Parameter tempPass temporary pass
     - Parameter univeristyName university name
     - Parameter universityId university ID
     - Parameter locationid incident Location ID
     - Parameter guardCode guard Code
     
     */
    
    init(fraternityName:String!, fraternityID:Int!, fraternitynickName:String!, subscripCode:String!, paymentStatus:String!, chapter:String!, tempPass:String!, univeristyName:String!, universityId:Int!, locationid:Int!, guardCode:String!){
        self.fraternityName = fraternityName
        self.fraternityID = fraternityID
        self.fraternitynickName = fraternitynickName
        self.subscripCode = subscripCode
        self.paymentStatus = paymentStatus
        self.chapter = chapter
        self.tempPass = tempPass
        self.univeristyName = univeristyName
        self.universityId = universityId
        self.locationid = locationid
        self.guardCode = guardCode
    }
    
    
    
}




