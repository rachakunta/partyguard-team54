//
//  ForgotPasswordViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 11/10/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This View controller implements forgot password functionality of basic user
class ForgotPasswordViewController: UIViewController {

    /// declaring variable for app delegate
    var appdelegate:AppDelegate!
    
    /// declaring variable for status code
    var statusCode1:Int!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Forgot Password"
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        hidingresetPasswrdFields(true)
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "forgotResponse:", name: "Data Delivered", object: nil)
      

        // Do any additional setup after loading the view.
    }

    //
    /**
     This function is an action for verifying the email and redirects it to change password screen
     - Parameter sender: sender of object - button
     */
    @IBAction func submitAction(sender: AnyObject) {
        if emailId.text == "" || emailId.text == nil
        {
            //displayAlertController("Input Alert", message: "Please enter your emailId")
            emailId.attributedPlaceholder = NSAttributedString(string: "Enter Email Id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if emailId.text != ""
        {
                let emailid:String = emailId.text!
                let statusOfValidation:Bool = validateEmail(emailid)
                if statusOfValidation == true
                {
                    let passwordurl = appdelegate.servicesDomainUrl+"api/ForgotPassword/ForgotPassword"
                    callingForgotPasswdService(["Email":emailId.text!], url: passwordurl)
                }
                else
                {
                    print("entered email is not correct")
                    displayAlertController("Input Alert", message: "Please enter valid email")
                }

        }
        
        
        
    }
    
    
    /**
     This function validates the format of the email.
     - Parameter String: enter email to be validated
     */
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(enteredEmail)
        
    }
    
    /// outlets for submit button
    @IBOutlet weak var submitBTN: UIButton!
    /// outlets for confirm password button
    @IBOutlet weak var confirmPassword: UITextField!
    /// outlets for new password button
    @IBOutlet weak var newPassword: UITextField!
    /// outlets for email id button
    @IBOutlet weak var emailId: UITextField!
    /// outlets for reset password button
    @IBOutlet weak var resetPasswordBTN: UIButton!
    
    
    
    /**
     This function is an action for password reset
     - Parameter sender: sender of object - button
     */
    @IBAction func resetPasswordAction(sender: AnyObject) {
        
        if newPassword.text == "" || newPassword.text == nil
        {
            //displayAlertController("Input Alert", message: "Please enter your new password")
            newPassword.attributedPlaceholder = NSAttributedString(string: "Enter New Password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if confirmPassword.text == "" || confirmPassword.text == nil
        {
            //displayAlertController("Input Alert", message: "Please confirm your password")
            confirmPassword.attributedPlaceholder = NSAttributedString(string: "Confirm New Password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else
        {
            let password:String = newPassword.text!
            let confirmPass:String = confirmPassword.text!
            print("entered email id correct")
            
            let statusOfpasswords = checkPasswords(password, confirmPwd: confirmPass)
            if statusOfpasswords == true
            {
                let passwordurl = appdelegate.servicesDomainUrl+"api/Account/NewPasswordSet"
                print("password url is \(passwordurl)")
                callserviceForPasswordReset(["Email":emailId.text!,"NewPassword":newPassword.text!,"ConfirmPassword":confirmPassword.text!], url: passwordurl )
                NSNotificationCenter.defaultCenter().addObserver(self, selector: "resetResponse:", name: "Data Delivered", object: nil)
            }

        }

        
    }
    
    
    /**
     This function validates weather the password and confirm password values are same
     - Parameter password: String entered in password
     - Parameter confirmPwd: String entered in confirm password feild.
     */
        func checkPasswords(password:String, confirmPwd:String)->Bool
        {
            if password ==  confirmPwd
            {
                return true
            }
            else{
                return false
            }
            
        }
        
    
    /**
     This function will hide the password and confirm password feilds
     - Parameter value: flag to hide or unhide the feilds
     */
    func hidingresetPasswrdFields(value:Bool)
    {
        newPassword.hidden = value
        confirmPassword.hidden = value
        resetPasswordBTN.hidden = value
    }
    
    /**
     This function will the email and submit feilds whne the email is validated
     
     */
    func hidingForgtPasswordFields()
    {
        emailId.hidden = true
        submitBTN.hidden = true
    }
    
    /**
     This function will call the services for forgot password
     - Parameter params: parameters for forget password service
     - Parameter url: url for forget password service
     */
    func callingForgotPasswdService(params : Dictionary<String, String>,url:String)
    {
        //print(params.debugDescription)
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print("request is \(request)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
                
            }).resume()
            
        }
        catch
        {
            print("something is wrong")
            //callbackforRegister()
        }

    }
    
    
    /**
     This function that calls the service for Password reset
     - Parameter params: parameters for password reset service
     - Parameter url: url for Passoword reset service
     */
    func callserviceForPasswordReset(params : Dictionary<String, String>,url:String)
    {
        print(params.debugDescription)
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print("request is \(request)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
                
            }).resume()
            
        }
        catch
        {
            print("something is wrong")
            //callbackforRegister()
        }
        
    }

    
    /**
     This function is an action which redirects the user to login screen
     - Parameter sender: sender of object - button
     */
    
    @IBAction func backToLoginAction(sender: AnyObject) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /**
     This function validates the whether the email id id valid or no
     - Parameter notification: to check the status of code
     */
    func forgotResponse(notification: NSNotification)
    {
        print("status code is \(statusCode1!)")
        if statusCode1! == 200
        {
            hidingForgtPasswordFields()
            
            hidingresetPasswrdFields(false)
            
        }
        else
        {
            displayAlertController("Email", message: "Entered email id is not valid, Please register your email id")
        }
    }

    /**
     This function will send confirmation dailog box whether the password is reset or no
     - Parameter notification: confirms if the password is resot or no
     */
    func resetResponse(notification: NSNotification)
    {
        print("status code is \(statusCode1!)")
        if statusCode1! == 200
        {
            print("status in if loop is \(statusCode1!)")
            displayAlertController1("Reset Alert", message: "Your password has been reset")
        }
        else
        {
            displayAlertController1("Reset Alert", message: "Some thing has gone wrong with the server")
        }
    }
    
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertController1(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertController(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in}))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
