//
//  GuardTabBarViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 9/27/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the functionality to redirect to the host or guard flow
class GuardTabBarViewController: UITabBarController {
    
    /// variable to know whether user is host or guard
    var hostOrGuard:String!
    /// appdelegate object
    var appdelegate:AppDelegate!
    /// object of login class
    var hostOrGuardloginObject:HostOrGuardLoginViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate

//        hostOrGuardloginObject = HostOrGuardLoginViewController()
//        print("host or guard login "+hostOrGuardloginObject.statusOfLogin)
//        hostOrGuard = hostOrGuardloginObject.statusOfLogin
//        let controller = self.storyboard?.instantiateViewControllerWithIdentifier("HostOrGuardLoginID") as! HostOrGuardLoginViewController
//        self.presentViewController(controller, animated: true, completion: nil)
//        hostOrGuard = controller.statusOfLogin
       // hostOrGuardloginObject = HostOrGuardLoginViewController()
        //print("host or guard is \(hostOrGuardloginObject.statusOfLogin)")
        if appdelegate.userDetails.userType == "guard"
        {
            if var tabBarController = self.viewControllers{
                let indexToRemove = 0
                print("index remove \(indexToRemove)")
                //var viewControllers = tabBarController.viewControllers
                print(" \(tabBarController.removeAtIndex(indexToRemove))")
                //self.tabBarController!.viewControllers = viewControllers
               self.setViewControllers(tabBarController, animated: true)
            }
        }
        

    
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
