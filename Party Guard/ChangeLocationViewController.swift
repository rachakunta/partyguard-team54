//
//  ChangeLocationViewController.swift
//  Party Guard
//
//  Created by Rachakunta,Naga Sri Harsha Yadav on 9/27/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the change location functionality for user
class ChangeLocationViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    /// logout services class
    var logoutClass:LogoutService!
    /// Text box text field
    @IBOutlet weak var textBox: UITextField!
    /// drop down picker view
    @IBOutlet weak var dropDown: UIPickerView!
    /// changed Location variable
    var changedLocation:String!
    ///status code variable
    var statusCode:Int!
    /// list of locations
    var list:[String] = []
    /// appdelagate object
    var appdelegate:AppDelegate!
    /// image object
    var retrievedImg:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Change Location"
        
        //loadView()
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        gettingUniversitiesList(appdelegate.servicesDomainUrl)
        dropDown.reloadAllComponents()
        addingLogoutBTN()
        //adding slide out menu functionality
        buttonLayout(setLocationBTN)
        
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
       
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        addingImageasRightBarButton()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    /// this method adds images as a right bar button
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(retrievedImg, forState: UIControlState.Normal)
        //add function for button
        button.addTarget(self, action: "imageButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        gettingUniversitiesList(appdelegate.servicesDomainUrl)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("populateUnivResponse:"), name: "Data Delivered", object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.navigationItem.title = "Change Location"
    }
    ///Log out button functionality
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }
    
    /// calls logout function
    func Logout_Functionality()
    {
        
        callingLogoutService()
       
    }
    /// Log out service function
    func callingLogoutService()
    {
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "LogoutResponse:", name: "Data Delivered", object: nil)
        logoutClass = LogoutService()
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
//        statusCode = logoutClass.statusCode1
//        print("code is \(statusCode)")
    }
    func populateUnivResponse(notification: NSNotification)
    {
        
    }
    /**
     This function gets the response for logout- confirmation
     - Parameter notification: confirmation for logout
     */
    func LogoutResponse(notification: NSNotification)
    {
        print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
           displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }
    
    /**
     Displays alert for the logout button click
     - paramete title : title of the alert box
     - parameter message: message for the alert box
     */
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
         self.presentViewController(alert, animated: true, completion: nil)
    }
    
    /**
     Method resposible for the button layout
     - paramete buttonName : UIButton object holding button name
     */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }
    
    /**
     Method resposible for getting university lists
     - paramete url : serice url
     */
    func gettingUniversitiesList(url:String)
    {
        var universityArray:[String] = []
        
        let requestURL = NSURL(string: url+"api/UniversityModels")
        let session:NSURLSession = NSURLSession.sharedSession()
        session.dataTaskWithURL(requestURL!,completionHandler: {(data, response, error)->Void in
           if response != nil
           {
            //var responseText:[NSDictionary]
            do{
                let responseText =  try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! [AnyObject]
                
                print("response text is \(responseText)")
                let size = responseText.count
                print("size is \(size)")
                if size == 0
                {
                    self.callbackForChangeLocation()
                }
                else
                {
                    print("response text is \(responseText)")
                    for eachuniversity in responseText
                    {
                        //print("each university is \(eachuniversity)")
                      let university = eachuniversity["univeristyName"] as! String
                        print(" university is \(university)")
                        universityArray.append(university)
                        
                    }
                self.list = universityArray
                    print("count is \(self.list.count) ")

                }
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
            }
            catch
            {
                print("Something is wrong")
                self.callbackForChangeLocation()
            }
            }
           
            
        }).resume()

    }
    
    /// Call back function for changing the location
    func callbackForChangeLocation()
    {
        if let path = NSBundle.mainBundle().pathForResource("universitiesList", ofType: "json")
        {
            if let jsonData = try? NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
            {
                if let jsonResult: NSArray = (try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)) as? NSArray
                {
                    
                    var universityArray:[String] = []
                    
                    let numberOfRows = jsonResult.count
                    print("size is \(numberOfRows)")
                    for item in jsonResult{
                       var id = item["ID"] as! String
                        var universityName = item["UniversityName"] as! String
                        print("university name is \(universityName)")
                        universityArray.append(universityName)
                    }
                    list = universityArray
                    //print("list size is \(list.count)")
                    dropDown.reloadAllComponents()
                }
            }
        }

    }
    //you can copy and paste this
    /// Button for location setting
    @IBOutlet weak var setLocationBTN: UIButton!
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = list[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.greenColor()])
        return myTitle
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        print("list ocunt is \(list.count)")
        return list.count
        
    }
    /**
     Method sets the location
     - parameter sender : anyObject object
    */
    @IBAction func setLocationAction(sender: AnyObject) {
        
        displayAlertController("Location alert", message:"your location has been set to \(changedLocation)")
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        //self.view.endEditing(true)
        print("list row is \(list[row])")
        return list[row]
        
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.textBox.text = self.list[row]
        changedLocation = textBox.text
        
        
        //self.dropDown.hidden = true
        
    }
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertController(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in}))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    /**
     This function enables the text field to edit
     
     - Parameter  textField: UITextField
     
     */
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == self.textBox {
            self.dropDown.hidden = false
            //if you dont want the users to se the keyboard type:
            
            textField.endEditing(true)
        }
        
        
    }

    

}
