//
//  HostAlertHistory.swift
//  Party Guard
//
//  Created by Harish K on 11/19/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import Foundation
/// This class is used to store details of alert history for host user
class HostAlertHistory
{
    /// outlet for basicUserfirstname textfield
    var basicUserfirstname:String!
    /// outlet for basicuserlastname textfield
    var basicuserlastname:String!
    /// outlet for fraternityName textfield
    var fraternityName:String!
    /// outlet for issueDate textfield
    var issueDate:String!
    /// outlet for issueID textfield
    var issueID:String!
    /// outlet for issueName textfield
    var issueName:String!
    /// outlet for comments textfield
    var comments:String!
    /// outlet for issueStatus textfield
    var issueStatus:String!
    /// outlet for issueLocation textfield
    var issueLocation:String!
    /// outlet for userimage textfield
    var userimage:String!
    
    /**
     This is a default constructor
     */
    init()
    {
        
    }
    
    
    /**
     This is a parameterized constructor
     - Parameter basicuser name of basic user
     - Parameter fraternityName name of the fraternity
     - Parameter issueDate issue generated date
     - Parameter issueID issue id
     - Parameter issueName name of an issue
     - Parameter comments comments on issue
     - Parameter issueStatus status of the issue
     - Parameter issueLocation location where issue taken place
     - Parameter basicuserlastname: last name of the basic user
     - Parameter userimage: image of the user
     
     */
    init(basicuser:String,fraternityName:String,issueDate:String,issueID:String,issueName:String,comments:String,issueStatus:String,issueLocation:String,basicuserlastname:String!,userimage:String!)
    {
        self.basicUserfirstname = basicuser
        self.fraternityName = fraternityName
        self.issueDate = issueDate
        self.issueID = issueID
        self.issueName = issueName
        self.comments = comments
        self.issueStatus = issueStatus
        self.issueLocation = issueLocation
        self.basicuserlastname = basicuserlastname
        self.userimage = userimage
        
        
    }
}
