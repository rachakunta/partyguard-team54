//
//  SelectFraternityViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 9/12/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the functionality of selecting fraternities from table view
class SelectFraternityViewController:UIViewController, UITableViewDataSource, UITableViewDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    /// ARRAY OF FRATERNITIES OBJECT
    var fraternitiesList = [Fraternity]()
    /// LOGOUT SERVICE OBJECT
    var logoutClass:LogoutService!
    //var passingValue:[String:[String]]!
    /// selected row object
    var selectedRow:Int!
    /// Tableview object
    @IBOutlet weak var tableViews: UITableView!
    ///table view object
    @IBOutlet weak var tblView: UITableView!
    ///table view object
    var tblv:UITableView!
    /// Userdetails object
    var userClass:UserDetails!
    /// appdelegate object
    var appdelegate:AppDelegate!
    /// image retrieved
    var retrievedImg:UIImage!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        addingLogoutBTN()
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        serviceForGettingFraternityDetails()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SelectFraternityViewController.fraternityResponse(_:)), name: "Data Delivered", object: nil)
        
        print("app delegate variable is \(appdelegate.servicesDomainUrl))")
        
        
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        addingImageasRightBarButton()
        
    }
    
    override func viewWillAppear(animated: Bool) {
      
    }
    /// adds image as right bar button
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(retrievedImg, forState: UIControlState.Normal)
        //add function for button
        button.addTarget(self, action: "imageButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    
  
    
    // retrieving fraternity list from restful api
    func serviceForGettingFraternityDetails()
    {
       
        let request = NSMutableURLRequest(URL: NSURL(string: appdelegate.servicesDomainUrl+"FraternityList")!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        
        do
        {
            print("do loop")
            request.setValue("Bearer "+appdelegate.accesstoken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                
                do
                {
                    let strData = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! [AnyObject]
                    var size = strData.count
                    print("size of response data \(size)")
                    if strData.count != 0
                    {
                       for eachItem in strData
                        {
                            print("each item is \(eachItem)")
                            let uniMode = eachItem["UniversityModel"]
                            //print("UNiversity abanme is : \(uniMode!!["univeristyName"] as! String)")
                            let fraternityName = eachItem["fraternityName"] as! String
                            let fraternityID = eachItem["fraternityID"] as! Int
                            let fraternitynickName = eachItem["nickName"] as! String
                            let subscripCode = eachItem["subscripCode"] as! String
                            let paymentStatus = eachItem["paymentStatus"] as! String
                            let chapter = eachItem["chapter"] as! String
                            let tempPass = eachItem["tempPass"] as! String
                            let univeristyName = uniMode!!["univeristyName"] as! String
                            print(univeristyName)
                            let universityId = eachItem["universityId"] as! Int
                            let locationid = eachItem["locationid"] as! Int
                            let guardCode = eachItem["guardCode"] as! String
                            let fraternity = Fraternity(fraternityName: fraternityName, fraternityID: fraternityID, fraternitynickName: fraternitynickName, subscripCode: subscripCode, paymentStatus: paymentStatus, chapter: chapter, tempPass: tempPass, univeristyName: univeristyName, universityId: universityId, locationid: locationid, guardCode: guardCode)
                            self.fraternitiesList.append(fraternity)
                            print("Fraternity is \(fraternity)")
                            
                            print("fraternity name is \(fraternityName)")
                        }
                        print("fraternity size is : \(self.fraternitiesList.count)")
                        
                    }
                    else
                    {
//                        self.callbackForProfile()
//                        self.dataResponse = 1
                    }
                    
                    dispatch_async(dispatch_get_main_queue()){
                        NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                    }
                   
                }
                catch
                {
                    print("soome wrong")
                }
                
                
            }).resume()
            
        }

    }
    /// adding logout button
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     Method displays number of secion in table view
     - Parameter tableView: UITableView table view object
     - returns: number of sections
     */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    /**
     Method displays number of rows in section
     - Parameter tableView: UITableView table view object\
     - Parameter section : Int
     returns: number of rows in sections
     */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("fraternities count in numberOfRowsInSection is : \(fraternitiesList.count)")
        return fraternitiesList.count
    }
    /**
     Method displays cells
     - Parameter tableView: UITableView table view object\
     - Parameter indexPath: NSIndexPath
     returns: number UITableViewCell
     */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("fraternitycell")!
        print("Fraternity lists cell")
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clearColor()
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel!
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        imgIcon.layer.borderWidth = 1.0
        imgIcon.layer.masksToBounds = false
        imgIcon.layer.borderColor = UIColor.blackColor().CGColor
        imgIcon.layer.cornerRadius = imgIcon.frame.size.height/2
        imgIcon.clipsToBounds = true
        
        imgIcon.image = UIImage(named: "party-security.jpg")
        lblTitle.text = fraternitiesList[indexPath.row].fraternityName  as String
        print(" in cellForRowAtIndexPath\(fraternitiesList.count)")
//        for i in fraternitiesList{
//            print(" in cellForRowAtIndexPath\(fraternitiesList.count)")
//            lblTitle.text = "yadav"//i.fraternityName
//        }
        
        
        tblv = tableView
        return cell

    }
    /**
     Method points to the cell selected
     - Parameter tableView: UITableView table view object\
     - Parameter indexPath: NSIndexPath
     returns: number UITableViewCell
     */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("didSelectRowAtIndexPath \(indexPath.row)")
        //tableView.reloadData()
        selectedRow = indexPath.row
        
    }
    /// sends data from one view controller to another
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if (segue.identifier == "fraternityToincidentSegue") {
            print("sample example \(self.tblv.indexPathForSelectedRow!.row))")
            
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destinationViewController as! IncidentsViewController
            // your new view controller should have property that will store passed value
            //NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
            //print("Selected Row is \(self.tableView.indexPathForSelectedRow)")
            viewController.selectedFraternity = fraternitiesList[self.tblv.indexPathForSelectedRow!.row]
        }


    }
    /// calls logout funtionality
    func Logout_Functionality()
    {
        
        callingLogoutService()
        
    }
    /// calls loutout services
    func callingLogoutService()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "LogoutResponse:", name: "Data Delivered", object: nil)
        logoutClass = LogoutService()
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
        //        statusCode = logoutClass.statusCode1
        //        print("code is \(statusCode)")
    }
    /**
     This function loads the fraternity service response
     - Parameter notification: notification from NSNotification
     */
    func fraternityResponse(notification: NSNotification)
    {
        tblView.reloadData()
    }
    /**
     This function loads the logout response
     - Parameter notification: notification from NSNotification
     */
    func LogoutResponse(notification: NSNotification)
    {
        //print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
            displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
