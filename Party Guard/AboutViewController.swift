//
//  AboutViewController.swift
//  Party Guard
//
//  Created by Addala,Satya Radha on 9/8/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
/// This view controller implements the know your app functionality
class AboutViewController: UIViewController {
    /// AV PLAYER VARIABLE
    var player: AVPlayer?
    
    
    /// outlet for Skip button
    @IBOutlet weak var skipBTN: UIButton!
    
    /*
     This is a function for button layout
     */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }
    
    /**
     logout button
     */
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }
    
    /*
      Function responsible for log out functionality
    */
    func Logout_Functionality()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Know your app"
        skipBTN.hidden = true
        addingLogoutBTN()
        //buttonLayout(skipBTN)
        // Do any additional setup after loading the view, typically from a nib.
        let videoURL: NSURL = NSBundle.mainBundle().URLForResource("Sample", withExtension: "mp4")!
        
        player = AVPlayer(URL: videoURL)
        player?.actionAtItemEnd = .None
        player?.muted = true
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.zPosition = -1
        let view1 = UIView(frame: CGRect(x: 50, y: 44, width: 340, height: 370))
        playerLayer.frame = view1.frame
        
        view.layer.addSublayer(playerLayer)
        
        player?.play()
        
        //loop video
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: "loopVideo",
                                                         name: AVPlayerItemDidPlayToEndTimeNotification,
                                                         object: nil)

        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Method responsible for looping he video
    func loopVideo() {
        player?.seekToTime(kCMTimeZero)
        player?.play()
    }

   

    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
