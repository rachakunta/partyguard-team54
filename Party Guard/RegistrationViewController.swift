//
//  RegistrationViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 8/31/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the registration functionality of basic user
class RegistrationViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    /// outlets for create account button
    @IBOutlet weak var createAccountBTN: UIButton!
     /// outlets for basic user image view
    @IBOutlet weak var basicUserIV: UIImageView!
     /// outlets for first name text field
    @IBOutlet weak var firstNameTF: UITextField!
     /// outlets for last name text field
    @IBOutlet weak var lastnameTF: UITextField!
     /// outlets for middle name text field
    @IBOutlet weak var middleNameTF: UITextField!
    
     /// outlets for address text field
    @IBOutlet weak var addressTF: UITextField!
    
     /// outlets for password text field
    @IBOutlet weak var passwordTF: UITextField!
     /// outlets for email text field
    @IBOutlet weak var emailTF: UITextField!
     /// outlets for confirm text field
    @IBOutlet weak var confirmPasswordTF: UITextField!
     /// outlets for mobile number text field
    
    
    @IBOutlet weak var mobileNoTF: UITextField!
    /// declaring variable for status code
    var statusCode1:Int!
    /// declaring variable for service domain url
    var servicesDomainUrl:String!
    
    
    //Read the image picked from UIImagePickerController
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        print("Image Selected")
        self.dismissViewControllerAnimated(true, completion: nil)
        basicUserIV.image = image
    }
    
    /**
     This function is an action for importing image from the app or from camera
     - Parameter sender: sender of object - button
     */
    @IBAction func importImage(sender: UIButton) {        let myActionSheet = UIAlertController(title: "Select image", message: "Where would you add images from?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        // choose from photos action button
        let chooseAction = UIAlertAction(title: "Choose from photos", style: UIAlertActionStyle.Default) { (action) in
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            image.allowsEditing = false
            self.presentViewController(image, animated: true, completion: nil)
        }
        //photo action button
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default) { (action) in
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.Camera
            image.allowsEditing = false
            self.presentViewController(image, animated: true, completion: nil)
        }
        
        // cancel action button
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) in
            print("Cancel action button tapped")
        }
        // add action buttons to action sheet
        myActionSheet.addAction(chooseAction)
        myActionSheet.addAction(takePhotoAction)
        myActionSheet.addAction(cancelAction)
        // support iPads (popover view)
        if let popoverController = myActionSheet.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
        }
        //myActionSheet.popoverPresentationController!.sourceView = self.
        //        myActionSheet.popoverPresentationController!.sourceRect = CGRectMake(self.addImageBTN.bounds.size.width / 2.0, self.addImageBTN.bounds.size.height / 2.0, 1.0, 1.0)
        
        //        myActionSheet.popoverPresentationController?.sourceView = self.view
        //        myActionSheet.popoverPresentationController?.sourceRect = self.view.bounds
        // present the action sheet
        self.presentViewController(myActionSheet, animated: true, completion: nil)
    }

    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        servicesDomainUrl = appdelegate.servicesDomainUrl
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        
        
        buttonLayout(createAccountBTN)
        
        // Do any additional setup after loading the view.
        //Set Default image
        basicUserIV.image = UIImage(named: "UserFemaleIcon.png")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "createAccountResponse:", name: "Data Delivered", object: nil)
    }
    /**
     This function adjusts the button edges to round corner
     - Parameter buttonName: button to be adjusted
     */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
    This function is an action which validates the user details and calls registraiton service
    - Parameter sender: sender of object - button
    */
  
    @IBAction func createAccountyAction(sender: AnyObject) {
        /// declaring a variable for storing converted mobile number 
        var convMobNo:String = ""
        
        print("create account button ")
        if firstNameTF.text == "" || firstNameTF.text == nil {
            print("first name validation \(firstNameTF.text)")
            
             firstNameTF.attributedPlaceholder = NSAttributedString(string: "Enter First Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            //displayAlertController1("Input Alert", message: "Please enter your First Name")
        }else if lastnameTF.text == "" || lastnameTF.text == nil {
            lastnameTF.attributedPlaceholder = NSAttributedString(string: "Enter Last Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
           // displayAlertController1("Input Alert", message: "Please enter your Last Name")
        }else if emailTF.text == "" || emailTF.text == nil {
            emailTF.attributedPlaceholder = NSAttributedString(string: "Enter Email Id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            //displayAlertController1("Input Alert", message: "Please enter Email address")
        }else if passwordTF.text == "" || passwordTF.text == nil {
            passwordTF.attributedPlaceholder = NSAttributedString(string: "Enter password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            //displayAlertController1("Input Alert", message: "Please enter Password")
        }else if confirmPasswordTF.text == "" || confirmPasswordTF.text == nil {
            confirmPasswordTF.attributedPlaceholder = NSAttributedString(string: "Confirm password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            //displayAlertController1("Input Alert", message: "Re-enter password")
        }else if mobileNoTF.text == "" || mobileNoTF.text == nil {
             mobileNoTF.attributedPlaceholder = NSAttributedString(string: "Enter mobile number", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
           // displayAlertController1("Input Alert ", message: "Please enter your phone number")
        }
        else
        {
            let password:String = passwordTF.text!
            let confirmPassword:String = confirmPasswordTF.text!
            print("entered email id correct")
            
            print(password)
            print(confirmPassword)
            if emailTF.text != ""
            {
                 let statusOfValidation:Bool = validateEmail(emailTF.text!)
                if statusOfValidation != true
                {
                    print("entered email is not correct")
                    displayAlertController1("Input Alert", message: "Please enter valid email")
                }
                
            }
            if password != ""
            {
                print("password loop outside")
                let isValid = validpassword(passwordTF.text!)
                if isValid != true
                {
                    print("password loop")
                   displayAlertController1("Input Alert", message: "Password should contain 1 uppercase, 1 digit, 1 special character and length should be min 8")
                }
                let statusOfpasswords = checkPasswords(password, confirmPwd: confirmPassword)
                if statusOfpasswords != true
                {
                    //displayAlertController1("Alert", message: "Your password has been reset")
                    
                    displayAlertController1("Input Alert", message: "Passwords did not match")
                }

            }
            
            if mobileNoTF.text != ""
            {
                let status:Bool = validate(mobileNoTF.text!)
                if status != true
                {
                    print("entered mobile no is not correct")
                       displayAlertController1("Input Alert", message: "Please enter only digits in format 333-333-3333")
                
                }
                else{
                    convMobNo = (mobileNoTF.text?.stringByReplacingOccurrencesOfString("-", withString: ""))!
                    post(["Email":addressTF.text!,"Password":passwordTF.text!,"ConfirmPassword":confirmPasswordTF.text!,"firstName":firstNameTF.text!,"lastName":lastnameTF.text!,"PhoneNumber":mobileNoTF.text!,"age":"24","userType":"basic"],url: servicesDomainUrl+"api/Account/Register")
                }
            }
            
        }
        
    }
    /**
     function validating the password entered by user
     - Parameter value: user entered password
     */
    func validpassword(value: String) -> Bool
    {
        print("validating password")
        let pass_REGEX = "^(?=.*\\d)(?=.*[A-Z])(?=.*\\W).{0,15}"
        //(?=.*[A-Z])(?=.*\d)(?=.*[_@$!%*?&#])([A-Za-z\dd$@$!%*?&#]).{0,15}"
        let passTest = NSPredicate(format: "SELF MATCHES %@", pass_REGEX)
        let result =  passTest.evaluateWithObject(value)
        print(result)
        return result
    }
    
    /**
     function validating the phone number entered by user
     - Parameter value: user entered mobile number
     */
    func validate(value: String) -> Bool
    {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluateWithObject(value)
        return result
    }
    /**
     function validating the email entered by user
     - Parameter enteredEmail: user entered email id
     */
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(enteredEmail)
        
    }
    
    /**
     This function check the passwords entered by user are matched or not
      - Parameter password: new password
      - Parameter password: confirm password
     */
    func checkPasswords(password:String, confirmPwd:String)->Bool
    {
        if password ==  confirmPwd
        {
            return true
        }
        else{
            return false
        }
        
    }
    

    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    
    func displayAlert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (UIAlertAction) in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
             let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }
        alert.addAction(action)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    
    func displayAlertController1(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("registrationVC") as! RegistrationViewController
//            self.presentViewController(nextViewController, animated: true, completion: nil)
            
        }))
        self.presentViewController(alert, animated: true) { 
            
        }
        
    }
    
    /**
     This function calls the registration service and parses the response 
     - Parameter params: dictionary with parameter names as keys and values of emaild, password and grant type
     - Parameter url: url of registration service url
     */
    func post(params : Dictionary<String, String>, url : String)
    {
        print(params.debugDescription)
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print("request is \(request)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
               
                
            }).resume()

        }
        catch
        {
            print("something is wrong")
            callbackforRegister()
        }
        //task.resume()
    }
    
    /**
     This function used to call after registration response from service 
     - Parameter notification: notification from NSNotification
     
     */
    func createAccountResponse(notification: NSNotification)
    {
        print("status code is \(statusCode1!)")
        if statusCode1! == 200
        {
            print("status in if loop is \(statusCode1!)")
            displayAlert("Registration alert", message: "Your account has been created")
        }
        else
        {
            displayAlertController1("Registration", message: "Some thing has gone wrong with the server")
        }
    }
    /// call back function for register functionality
    func callbackforRegister()
    {
        displayAlert("Registration alert", message: "Your account has been created")
    }
}


 