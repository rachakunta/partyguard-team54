//
//  HiostoryViewController.swift
//  Party Guard
//
//  Created by Rachakunta,Naga Sri Harsha Yadav on 9/27/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the functionality of history of requests for guard user/ host user
class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /// ourlet for table view
    @IBOutlet weak var requestHistTableV: UITableView!
    
    
    /// array which stores the objects of type HostAlertHistory
    var historyAlert:[HostAlertHistory] = []
    
    /// logoutClass holds the object of type LogoutService
    var logoutClass:LogoutService!
    
    /// this variable is used to call Appdelegate class
    var appdelegate:AppDelegate!
    
    /// this variable is used to call 
    var tblv:UITableView!
    var retrievedImg:UIImage!
    var hostAlerClass = HostAlertHistory()
    
    override func viewDidLoad() {
        self.navigationItem.title = "History"
        //addSlideMenuButton()
        super.viewDidLoad()
        addingLogoutBTN()
         let alert1 = HostAlertHistory(basicuser: "Harish", fraternityName: "Alpha Kappa", issueDate: "11/19/2016", issueID: "4", issueName: "Fight", comments: "Need Help", issueStatus: "Resolved", issueLocation: "Deck", basicuserlastname: "K", userimage: "http://userimage/slash")
            //        let alert2 = Alert(userImage: "fraternityIcon", userName: "Michelle Lin", userLocation: "Second Floor")
//        let alert3 = Alert(userImage:"fraternityIcon", userName: "Melinda Rose", userLocation: "First Floor")
//        let alert4 = Alert(userImage: "fraternityIcon", userName: "Annie lu", userLocation: "Second Floor")
        //let alert1 = Alert(userImage:"fraternityIcon", userName: "Kathryn rose", userLocation: "First Floor", comments: "Please come with weapons lets fight", alertType: "intruder", fraternityName: "Kappa Gamma sigma")
        //let alert2 = Alert(userImage: "fraternityIcon", userName: "Michelle Lin", userLocation: "Second Floor" , comments: "Please come with weapons lets fight", alertType: "intruder", fraternityName: "Kappa Gamma sigma")
        //let alert3 = Alert(userImage:"fraternityIcon", userName: "Kathryn rose", userLocation: "First Floor", comments: "Please come with weapons lets fight", alertType: "intruder", fraternityName: "Kappa Gamma sigma")
        //let alert4 = Alert(userImage: "fraternityIcon", userName: "Michelle Lin", userLocation: "Second Floor" , comments: "Please come with weapons lets fight", alertType: "intruder", fraternityName: "Kappa Gamma sigma")
        // alerts
        //alerts.append(alert1)
        //alerts.append(alert2)
        //alerts.append(alert3)
        //alerts.append(alert4)
        historyAlert.append(alert1)
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        addingImageasRightBarButton()
        // Do any additional setup after loading the view.
    }
    
    ///This method adds the button with background as user image
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(retrievedImg, forState: UIControlState.Normal)
        //add function for button
        button.addTarget(self, action: "imageButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    override func viewDidAppear(animated: Bool) {
        self.navigationItem.title = "My Alerts"

    }
   
    ///This method adds the button at the top right corner in navigation item
    
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /** 
     This method displays the number of sections in the tableview
     
     :param: tableview
     returns: number of sections
     
     */
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    /**
     This method displays the number of rows in sections in the tableview
     
     :param: tableview
     :params: section
     returns: number of rows in sections
     
     */
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyAlert.count
    }
    /**
     This method displays the cellForRowAtIndexPath in the tableview
     
     :param: tableview
     :params: indexPath
     returns: UITableViewCell
     
     */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("HistoryReqcell")!
        let imgIcon : UIImageView = cell.contentView.viewWithTag(101) as! UIImageView
        let userLbl : UILabel = cell.contentView.viewWithTag(102) as! UILabel!
        let locationLbl : UILabel = cell.contentView.viewWithTag(103) as! UILabel!
        
        imgIcon.layer.borderWidth = 1.0
        imgIcon.layer.masksToBounds = false
        imgIcon.layer.borderColor = UIColor.blackColor().CGColor
        imgIcon.layer.cornerRadius = imgIcon.frame.size.height/2
        imgIcon.clipsToBounds = true
        
        imgIcon.image = UIImage(named: "Profile.png")!
        //print(alerts[indexPath.row].userName)
        userLbl.text = historyAlert[indexPath.row].basicUserfirstname + " " + historyAlert[indexPath.row].basicuserlastname
        locationLbl.text = historyAlert[indexPath.row].issueLocation
        //
        tblv = tableView
        return cell
    }
    /** this method transefer data from one view controller to another
     -Parameter : segue: UIStoryboardSegue
     -Parameter : sender: AnyObject?
 
     */
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        print("//////////////////////////////////")
        if (segue.identifier == "historyTohistoryDetailSegue") {
            print("----****+++++===============================================")
            let historyAlertDetailVC = segue.destinationViewController as! HistoryAlertDetailViewController
            print()
            historyAlertDetailVC.historyDetails = historyAlert[self.tblv.indexPathForSelectedRow!.row]
        }
    }
    
    /// This function is used to redirect to the login screen once user clicks on logout button
    
    func Logout_Functionality()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
        //callingLogoutService()
        
    }
    
    /// calling logout service to handle logout functionality
    
    func callingLogoutService()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "LogoutResponse:", name: "Data Delivered", object: nil)
        logoutClass = LogoutService()
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
        //        statusCode = logoutClass.statusCode1
        //        print("code is \(statusCode)")
    }
    
    
    /**
     This function displays the alerts once the user logout from the app depends upon the status
     
     - Parameter  notification: notification from NSNotification
    */
    func LogoutResponse(notification: NSNotification)
    {
        print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
            displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }
    
    /**
     This function displays the alerts with a message based on user action and redirects to login screen
     
      - Parameter  title: title of alert
      - Parameter  message: description of alert
     */
    
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
