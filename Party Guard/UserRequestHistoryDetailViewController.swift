//
//  UserRequestHistoryDetailViewController.swift
//  Party Guard
//
//  Created by Harish K on 10/11/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the request history details functionality of basic user 
class UserRequestHistoryDetailViewController: UIViewController {
    
    /// fraternity name label
    @IBOutlet weak var fraternityName: UILabel!
    
   
    /// status button
    @IBOutlet weak var statusBTN: UIButton!
    
    
    /// incident label
    @IBOutlet weak var incident: UILabel!
    
    
    /// location label
    @IBOutlet weak var location: UILabel!
    
    
    /// university label
    @IBOutlet weak var university: UILabel!
    
    /** function for button layout
     - Parameter buttonName:UIButton
     */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }
    
    
    /** function to undo the action
     - Parameter sender: AnyObject
     */
    @IBAction func revertAction(sender: AnyObject) {
        
        if statusBTN.titleLabel?.text == "Resolved"
        {
            displayAlertController("Request Alert", message:"This request has already been resolved")
            statusBTN.backgroundColor = UIColor.greenColor()
        }
        else if statusBTN.titleLabel?.text == "Revert"
        {
            let alert = UIAlertController(title: "Are you sure you want to revert your request?", message: "", preferredStyle: .Alert)
                       //        alert.addTextFieldWithConfigurationHandler(configurationTextField)
            //        alert.addTextFieldWithConfigurationHandler(configurationTextField)
            //
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:handleCancel))
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: handleDone))
            self.presentViewController(alert, animated: true, completion: {
                print("completion block")
            })

        }
    }
    
    
    
    override func viewDidAppear(animated: Bool) {
        
        
        if statusBTN.titleLabel?.text == "Resolved"
        {
            statusBTN.backgroundColor = UIColor.greenColor()
        }
        else if statusBTN.titleLabel?.text == "Revert"
        {
            statusBTN.backgroundColor = UIColor.redColor()
        }

    }
    
    /**
     method responsible for cancel button
     - Parameter alertView: UIAlertAction!
     */
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }

    /**
     method responsible for Done button
     - Parameter alertView: UIAlertAction!
     */
    func handleDone(alertView: UIAlertAction!){
        let alert1 = UIAlertController(title: "Your request has been cancelled successfully!!", message: "", preferredStyle: .Alert)
        alert1.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in
            //            let secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("historyviewcontroller") as! HistoryViewController
            //            self.navigationController?.pushViewController(secondViewController, animated: true)
            //            self.openViewControllerBasedOnIdentifier("historyviewcontroller")
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("RequestsVC") as! UserRequestHistoryViewController
//            self.presentViewController(nextViewController, animated: true, completion: nil)
//            print("completion block in Done")
        }))
        self.presentViewController(alert1, animated: true, completion: nil)
        
    }

    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertController(title:String, message:String)
    {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in}))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

    /// time label
    @IBOutlet weak var time: UILabel!
    
    /// comments label
    @IBOutlet weak var comments: UILabel!
    
    /// selected request variable 
    var selectedRequest:UserRequest!
    override func viewDidLoad() {
        super.viewDidLoad()
        checkingstausOfbutton()
        buttonLayout(statusBTN)
        print(selectedRequest.comments)
        self.fraternityName.text = selectedRequest.fraternity
        self.statusBTN.setTitle(selectedRequest.status, forState: UIControlState.Normal)
        self.incident.text = selectedRequest.incident
        self.location.text = selectedRequest.location
        //self.university.text = selectedRequest.university
        //self.time.text = selectedRequest.time
        self.comments.text = selectedRequest.comments
        // Do any additional setup after loading the view.
    }

    /// cheks the status with the button label
    func checkingstausOfbutton()
    {
        if statusBTN.titleLabel?.text == "Resolved"
        {
            statusBTN.backgroundColor = UIColor.greenColor()
        }
        else if statusBTN.titleLabel?.text == "Revert"
        {
            statusBTN.backgroundColor = UIColor.redColor()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
