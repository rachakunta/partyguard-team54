//
//  LocationViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 8/31/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the functionality of user launch request 
class LocationViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    /// array of strings which has different locations in a fraternity
  var LocationList:[String] = ["Upstairs", "Main Floor", "Basement", "Deck"]
    
    /// declaring a varible for storing an incident in a faternity
  var incident  = ""
    
    /// declaring a varible for holding fraternity class
    var selectedFraternity:Fraternity!
    
    /// declargin a variable for holding appdelegate class
    var appdelegate:AppDelegate!
    
    /// variable to hold location
    var location:String!
    
    /// declaring a variable to hold the userrequest object
    var requestClass:UserRequest!
    
    /// outlet for fraternity image
    @IBOutlet weak var fraternityIMG: UIImageView!
    /// outlet for fraternity label
    @IBOutlet weak var fraternityLBL: UILabel!
    
    /// variable to hold the status code
    var statusCode1:Int!
    
    /// outlet for incident
    @IBOutlet weak var incidentLBL: UILabel!
    override func viewDidLoad() {
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        incidentLBL.text = incident
        print("------\(LocationList.count)")
        print("in locationviewcontroller \(LocationList.count)")
        super.viewDidLoad()
       
        adjustingView()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LocationList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("LocationCell")! as UITableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.layoutMargins = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clearColor()
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel!
        
        lblTitle.text = LocationList[indexPath.row]
       
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Final selected things are...:-------------------")
        print(selectedFraternity.fraternityName)
        print(incident)
        print(LocationList[indexPath.row])
        location = LocationList[indexPath.row]
        requestClass = UserRequest(date: "\(NSDate())", incident: incident, fraternity: selectedFraternity.fraternityName, location: LocationList[indexPath.row], comments: "", university: "NWMSU", status: "Unresolved")
        
        print("------------------------------------------------")
        let alert = UIAlertController(title: "Are you sure you want to rise a request?", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: .Default, handler:handleDone))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })
             }
    
    var tField: UITextField!
    
    /**
     This function used to place the placeholder in comments textfield
     - Parameter textField: comments textfield
     */
    
    func configurationTextField(textField: UITextField!)
    {
        print("generating the TextField")
        textField.placeholder = "Comments"
        tField = textField
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "requestHistory") {
            //print("sample example \(self.tblv.indexPathForSelectedRow!.row))")
            
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destinationViewController as! UserRequestHistoryViewController
            // your new view controller should have property that will store passed value
            //NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
            //print("Selected Row is \(self.tableView.indexPathForSelectedRow)")
            viewController.userRequestClass = requestClass
        }

    }
    /**
     This function used to handle cancle button from alert
     - Parameter alertView: current alert action
 */
    func handleCancel(alertView: UIAlertAction!)
    {
        print("Cancelled !!")
    }
    
    /// function to display alert
    func displayAlert()
    {
        print("Got it harsha")
        
        let alertController = UIAlertController(title: "iOScreator", message:
            "Hello, world!", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    /**
     This function is an action when user clicks on emergency button with sender object as button
     - Parameter sender: sender of object in this case it is button
     */
    @IBAction func emergencybutton(sender: AnyObject) {
        let alert = UIAlertController(title: "Are you sure you want to rise an Emergency request?", message: "", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: .Default, handler: handleDone))
        self.presentViewController(alert, animated: true, completion: {
            print("completion block")
        })

        
    }
    
    /**
     This function is used to handle the done action when user clicks on done button in alert
     - Parameter alertView: sending current alert action as a parameter
     */
    func handleDone(alertView: UIAlertAction!){
        print("Comments on the issueSubmission\(tField.text!)")
        issueSubmission(["issueName":incident, "issueLocation":location, "issueDate": "\(NSDate())", "comments": tField.text!, "fraternityName" : selectedFraternity.fraternityName], url: appdelegate.servicesDomainUrl+"api/IssuesModels")
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LocationViewController.fraternityResponse(_:)), name: "Data Delivered", object: nil)
    }
    
    /// outlet for fraternity imageview
    @IBOutlet weak var fraternityImageIV: UIImageView!
    
    /// adjusting the frarnity image view - making the image as circle shape
    func adjustingView()
    {
        fraternityImageIV.layer.borderWidth = 1.0
        fraternityImageIV.layer.masksToBounds = false
        fraternityImageIV.layer.borderColor = UIColor.blackColor().CGColor
        fraternityImageIV.layer.cornerRadius = fraternityImageIV.frame.size.height/2
        fraternityImageIV.clipsToBounds = true
    }
    
    
    
    
    /**
     This function is used to launch the request after selecting fraternity, issue and location by sending post request to service
     - Parameter params: dictionary which holds all the request details as key value pairs
     - Parameter url: url of issue submission
     */
    func issueSubmission(params : Dictionary<String, String>, url : String)
    {
        
        print("Current Date is \(NSDate())")
        print("parameters in calling update profile service \(params.debugDescription)")
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.setValue("Bearer "+appdelegate.accesstoken, forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print("request is LVC \(request)")
            print("request body is LVC \(request.debugDescription)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response of issueSubmission LVC: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                //print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is LVC \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
                
            }).resume()
            
        }
        catch
        {
            print("something is wrong")
            
        }
        
    }
    
    /**
     This function displays alerts to the user based on response from the service
     - Parameter notification: notification from NSNotification
     */
    func fraternityResponse(Notification:NSNotification){
        print("Status code is finally \(statusCode1)")
        if statusCode1 == 200 {
            let alert1 = UIAlertController(title: "Your request has been raised successfully!!", message: "", preferredStyle: .Alert)
            alert1.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in
                //            let secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("historyviewcontroller") as! HistoryViewController
                //            self.navigationController?.pushViewController(secondViewController, animated: true)
                //            self.openViewControllerBasedOnIdentifier("historyviewcontroller")
                //performSegueWithIdentifier("requestHistory", sender: nil)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserHomeVC") as! UserTabBarViewController
//                
//                let nav:UINavigationController = self.tabBarController!.viewControllers![1] as! UINavigationController
//                let reqVC = UserRequestHistoryViewController()
//                reqVC.userRequestClass = self.requestClass
//                let vc = self.tabBarController?.viewControllers![1] as! UserRequestHistoryViewController
//                vc.userRequestClass = self.requestClass
                self.presentViewController(nextViewController, animated: true, completion: nil)
                
                print("completion block in Done")
                
            }))
            self.presentViewController(alert1, animated: true, completion: nil)
            
        }
        else{
            print("There is a problem dood!!!!")
            let alert1 = UIAlertController(title: "Sorry!! There is a problem in issue Submission", message: "", preferredStyle: .Alert)
            alert1.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in
               let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserHomeVC") as! UserTabBarViewController
                var vc = self.tabBarController?.viewControllers
                self.presentViewController(nextViewController, animated: true, completion: nil)
                print("completion block in Done")
                
            }))
            self.presentViewController(alert1, animated: true, completion: nil)
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
