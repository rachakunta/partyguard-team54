//
//  AlertViewController.swift
//  Party Guard
//
//  Created by Rachakunta,Naga Sri Harsha Yadav on 9/27/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the listing of alerts for guard and host as well
class AlertViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

     /// outlets for table view
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var UserIV: UIImageView!
    
    /// declaring variable for alerts
    var alerts:[Alert] = []
    /// declaring variable for tblv
    var tblv:UITableView!
   
    /// declaring variable for logout class
    var logoutClass = LogoutService()
    
    /// declaring variable for appdelegate
    var appdelegate:AppDelegate!
    
    /// declaring variable for retrieved image
    var retrievedImg:UIImage!
    
    
    override func viewDidLoad() {
//        let alert1 = Alert(userImage:"fraternityIcon", userName: "Kathryn rose", userLocation: "First Floor", comments: "Please come with weapons lets fight", alertType: "intruder", fraternityName: "Kappa Gamma sigma")
//        let alert2 = Alert(userImage: "fraternityIcon", userName: "Michelle Lin", userLocation: "Second Floor" , comments: "Please come with weapons lets fight", alertType: "intruder", fraternityName: "Kappa Gamma sigma")
        //alerts
        self.navigationItem.title = "Unclaimed Alerts"
        addingLogoutBTN()
        //alerts.append(alert1)
        //alerts.append(alert2)
        super.viewDidLoad()
        print("viewdidload")
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        serviceForGettingAlerts()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AlertViewController.guardAlertResponse(_:)), name: "Data Delivered", object: nil)
        
        
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        addingImageasRightBarButton()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()

        self.navigationItem.title = "Unclaimed Alerts"
        addingLogoutBTN()
        //alerts.append(alert1)
        //alerts.append(alert2)
        super.viewDidLoad()
        print("viewdidload")
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        serviceForGettingAlerts()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AlertViewController.guardAlertResponse(_:)), name: "Data Delivered", object: nil)
        
        
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        addingImageasRightBarButton()
        // Do any additional setup after loading the view.
        
    }
    
    
    /**
     This function adds images to right bar button
     */
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(retrievedImg, forState: UIControlState.Normal)
        //add function for button
        button.addTarget(self, action: "imageButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    override func viewDidAppear(animated: Bool) {
        self.navigationItem.title = "Unclaimed Alerts"
        print("-----------------------------------------------")
        alerts = []
    }

    
    /**
     This function adds the logout button explicitly on the screen
     - Parameter buttonName: button to be adjusted
     */
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }
    
//    func Logout_Functionality()
//    {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
//        self.presentViewController(nextViewController, animated: true, completion: nil)
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(alerts.count)
        return alerts.count
        
    }
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        
//    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        tblv = tableView
        let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("alertcell")!
        let imgIcon : UIImageView = cell.contentView.viewWithTag(103) as! UIImageView
        let userLbl : UILabel = cell.contentView.viewWithTag(101) as! UILabel!
        let locationLbl : UILabel = cell.contentView.viewWithTag(102) as! UILabel!
        
        imgIcon.layer.borderWidth = 1.0
        imgIcon.layer.masksToBounds = false
        imgIcon.layer.borderColor = UIColor.blackColor().CGColor
        imgIcon.layer.cornerRadius = imgIcon.frame.size.height/2
        imgIcon.clipsToBounds = true
        
        imgIcon.image = UIImage(named: "ProfileIcon.png")!
        print(alerts[indexPath.row].basicUserFirstName)
        userLbl.text = alerts[indexPath.row].basicUserFirstName + " " + alerts[indexPath.row].basicUserLastName
        locationLbl.text = alerts[indexPath.row].issueLocation
        
        return cell
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        print("didselectatrowindexpath \(indexPath.row)")
//        let alertDetailVC: AlertDetailViewController = AlertDetailViewController()
//        alertDetailVC.alertDetails = alerts[indexPath.row]
//        print(alertDetailVC.alertDetails.comments)
//    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        print("//////////////////////////////////")
        if (segue.identifier == "alertToAlertDetailSegue") {
            print("----****+++++===============================================")
            let alertDetailVC = segue.destinationViewController as! AlertDetailViewController
            print()
            alertDetailVC.alertDetails = alerts[self.tblv.indexPathForSelectedRow!.row]
        }
    }

    

    /**
     This function logs out the user from app and displays the login screen
     */
    func Logout_Functionality()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
        //callingLogoutService()
        
    }
    
    /**
     This function calls the logout service
     */
    func callingLogoutService()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AlertViewController.LogoutResponse(_:)), name: "Data Delivered", object: nil)
        
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
        //        statusCode = logoutClass.statusCode1
        //        print("code is \(statusCode)")
    }
    
    
    /**
     This function gets the service for getting the alerts
     */
    func serviceForGettingAlerts(){
        let request = NSMutableURLRequest(URL: NSURL(string: appdelegate.servicesDomainUrl+"api/IssueList")!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        
        do
        {
            print("do loop")
            request.setValue("Bearer "+appdelegate.accesstoken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                let status = httpResponse?.statusCode
                if status == 404{
                    print("No Alerts")
                    self.displayAlert("Alert", message: "Currently you have no alerts!!")
                }
                if status == 200
                {
                
                do
                {
                    let strData = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! [AnyObject]
                    var size = strData.count
                    print("size of response data \(size)")
                    print("Harsha yadav Racha \(response)")
                    //self.tableView.reloadData()
                    
                    if strData.count != 0
                    {
                        for eachItem in strData
                        {
                            print("each item is \(eachItem)")
                            let temp = eachItem["BasicUserProfileModel"]
                            let uniMode = temp!!["UniversityModel"]
                            //print("UNiversity abanme is : \(uniMode!!["univeristyName"] as! String)")
                            let universityName = uniMode!!["univeristyName"] as! String
                            print("harsha yadav man \(universityName)")
                            let universityID = uniMode!!["universityID"] as! Int
                            let basicUserID = temp!!["basicUserID"] as! Int
                            let basicUserLastName = temp!!["lastName"] as! String
                            let basicUserFirstName = temp!!["firstName"] as! String
                            //let imgUrl = temp!!["imgUrl"] as! String
                            let issueID = eachItem["issueID"] as! Int
                            let issueName = eachItem["issueName"] as! String
                            let issueLocation = eachItem["issueLocation"] as! String
                            let issueDate = eachItem["issueDate"] as! String
                            let comments = eachItem["comments"] as! String
                            let fraternityName  = eachItem["fraternityName"] as! String
                            print("Data of harshaaaa \(basicUserLastName)")
                            
                            let alert = Alert(universityName: universityName, universityID: universityID, basicUserID: basicUserID, basicUserLastName: basicUserLastName, basicUserFirstName: basicUserFirstName, imgUrl: "sample image url", issueID: issueID, issueName: issueName, issueLocation: issueLocation, issueDate: issueDate, comments: comments, fraternityName: fraternityName)
                            //let fraternity = Fraternity(fraternityName: fraternityName, fraternityID: fraternityID, fraternitynickName: fraternitynickName, subscripCode: subscripCode, paymentStatus: paymentStatus, chapter: chapter, tempPass: tempPass, univeristyName: univeristyName, universityId: universityId, locationid: locationid, guardCode: guardCode)
                            //self.fraternitiesList.append(fraternity)
                            //print("Fraternity is \(fraternity)")
                            self.alerts.append(alert)
                            print("guard alerts are \(alert)")
                            
                            //print("fraternity name is \(fraternityName)")
                        }
                        //print("fraternity size is : \(self.fraternitiesList.count)")
                        
                    }
                    else
                    {
                        //                        self.callbackForProfile()
                        //                        self.dataResponse = 1
                    }
                    
                    dispatch_async(dispatch_get_main_queue()){
                        NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                    }
                    
                }
                catch
                {
                    print("soome wrong")
                }
            }
            else
                {
                    self.displayAlert("Alert", message: "Currently you have no Requests")
                }
            
            }).resume()
                
           
            
        }

        
    }
    
    /**
     This function gets the response for logout- confirmation
     - Parameter notification: confirmation for logout
     */
    func LogoutResponse(notification: NSNotification)
    {
        print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
            displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }
    
    /**
     This function displays the alerts with a message based on user action for logout
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            //self.presentViewController(alert, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    
    /**
     function reloads the table view for alerts
     - Parameter notification: responce for alerts
     */
    func guardAlertResponse(notification: NSNotification){
        tableView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
