//
//  LoginViewController.swift
//  Party Guard
//
//  Created by Kola,Harish on 8/31/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
///This view controller implements login functionality of basic user
class LoginViewController: UIViewController{
    
    
    /// outlet for username textfield
    
    @IBOutlet weak var usernameTF: UITextField!
    
    /// outlet for login button
    
    @IBOutlet weak var loginBtn: UIButton!
    
    /// outlet for create account button
    
    @IBOutlet weak var createAccountBtn: UIButton!
    
    /// variable to store access token of user
    var access_token:String!
    
    /// variable to store app delegate class
    var appdelegate:AppDelegate!
    //Testing on JSON
    
    var NumberOfRows = 0
    var latitude:Double = 0
    var longitude:Double = 0
    var country:String = ""
    var capital:String = ""
    var color:String = ""
    
    /// this variable holds the service domain url
    var servicesDomainUrl:String!
    
    /// this variale holds the error status
    var errorStatus:Int!
    
    /// this variable hodls the userDetails object
    var userClass:UserDetails!
    
    
    
    /// outlet for password textfield
    @IBOutlet weak var passwordTF: UITextField!
    override func viewDidLoad() {
        usernameTF.clearButtonMode = UITextFieldViewMode.WhileEditing
        passwordTF.clearButtonMode = UITextFieldViewMode.WhileEditing
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        
        //print("app delegate variable is \(appdelegate.servicesDomainUrl))")
        servicesDomainUrl = appdelegate.servicesDomainUrl
        
        
        // Do any additional setup after loading the view.
        
        buttonLayout(loginBtn)
        buttonLayout(createAccountBtn)
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    /**
     This function adjusts the button edges to round corner
     - Parameter buttonName: button to be adjusted
     */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }
    
    /**
     This function is an action which performs the validation and calls the login service
     - Parameter sender: sender of object - button
     */
    @IBAction func loginAction(sender: AnyObject)
    {
        if  usernameTF.text == "" || usernameTF.text == nil
        {
            displayAlertController("Login alert", message:"Please enter your username")
        }
        else if passwordTF.text == "" || passwordTF.text == nil
        {
            displayAlertController("Login alert", message:"Please enter your password")
        }
        else
        {
            let url = servicesDomainUrl+"token"
            print("url is \(url)")
            post(["username":usernameTF.text!,"password":passwordTF.text!,"grant_type":"password"],url: url)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.loginResponse(_:)), name: "Data Delivered", object: nil)
        }
        
        
    }
    
    
    /**
     This function calls the login service and parses the response to retrieve the access token of a login user
     - Parameter params: dictionary with parameter names as keys and values of emaild, password and grant type
     - Parameter url: url of login service url
     */
    func post(params : Dictionary<String, String>, url : String)
    {
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        var bodyData = ""
        for (key,value) in params{
            if (value == ""){ continue }
            let scapedKey = key.stringByAddingPercentEncodingWithAllowedCharacters(
                .URLHostAllowedCharacterSet())!
            let scapedValue = value.stringByAddingPercentEncodingWithAllowedCharacters(
                .URLHostAllowedCharacterSet())!
            bodyData += "\(scapedKey)=\(scapedValue)&"
            
            
        }
        let data1:String = bodyData.substringToIndex(bodyData.endIndex.predecessor())
        print("body data is   \(data1)")
        do
        {
            print("do loop")
            try request.HTTPBody = data1.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
            request.addValue("text/plain", forHTTPHeaderField: "Accept")
            print("request is \(request.debugDescription)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                //print("Response: \(response)")
                //                let strData = NSString(data: data!, encoding: NSUTF8StringEncoding) as! AnyObject
                //var normalResponse:NSURLResponse = nil
                if response != nil
                {
                    var strData:[String:AnyObject]!
                    do
                    {
                        try strData = NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as AnyObject! as! [String : AnyObject]
                        //print(strData.count)
                        //print("Body: \(strData)")
                        if strData["error_description"] != nil
                        {
                            self.errorStatus = 1
                        }
                        else
                        {
                            self.errorStatus = 0
                            //print("access token is \(strData["access_token"]!)")
                            self.access_token = strData["access_token"]! as! String
                            
                            
                            // calling user service class object and populating user details class with the retreived data from api
                            
                            
                            //pprint("user firstname from user class is \(self.appdelegate.userDetails.firstName!)")
                            print("userName is \(strData["userName"]!)")
                            self.appdelegate.accesstoken = self.access_token
                            self.gettingAccessToken(self.access_token)
                            
                            // calling user details api with the retrieved access token and url
                            
                        }
                        
                        dispatch_async(dispatch_get_main_queue()){
                            NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                        }
                        
                        //               if let accesstoken = strData["access_token"] as! String
                        //               {
                        //                print("access token is \(accesstoken)")
                        //                }
                    }
                    catch
                    {
                        print("some wrong --  calling call back functions ")
                        self.callbackforLogin()
                    }
                }
                
                
                
                
                
            }).resume()
            
        }
        //task.resume()
    }
    
    /// aleternate function for login incase of login service failure
    func callbackforLogin()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserHomeVC") as! UserTabBarViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
        
    }
    
    func gettingAccessToken(accessTok:String)->String
    {
        //print("access is \(accessTok)")
        return accessTok
    }
    
    /**
     This function displays alerts to user with title and appropriate message
     - Parameter title: title of alert
     - Parameter message: description of alert
     
     */
    func displayAlertController(title:String, message:String)
    {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in}))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    
    
    /**
     This function used to call after login response from service by using error status and redirecting to login screen based on response
     - Parameter notification: notification from NSNotification
     
     
     */
    func loginResponse(notification:NSNotification)
    {
        
        //appdelegate.accesstoken = gettingAccessToken(<#T##accessTok: String##String#>)
        if errorStatus == 1
        {
            displayAlertController("Login Alert", message: "Incorrect Username/password")
            
        }
        else if errorStatus == 0
        {
            //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.inforesponse(_:)), name: "Data Delivered", object: nil)
            //self.userServiceClass = UserDetailsServiceClass()
            callServForUserDetails(self.access_token, url: self.appdelegate.servicesDomainUrl+"api/Account/UserInfo")
            //print(userServiceClass.userClass.emailId)
            //
            //         print("access tok is \(appdelegate.accesstoken)")
            //            print("Email id is \(appdelegate.userDetails.emailId)")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserHomeVC") as! UserTabBarViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
            
        }
        
        
    }
    //    func inforesponse(notification:NSNotification)
    //    {
    //
    //            //self.userServiceClass = UserDetailsServiceClass()
    //            //callServForUserDetails(self.access_token, url: self.appdelegate.servicesDomainUrl+"api/Account/UserInfo")
    //            //print(userServiceClass.userClass.emailId)
    //
    //            print("access tok is \(appdelegate.accesstoken)")
    //            print("Email id is \(appdelegate.userDetails.emailId)")
    //
    //        }
    
    
    
    
    //JSON Testing
    //    func parseJson(){
    //        if let path = NSBundle.mainBundle().pathForResource("sample", ofType: "json")
    //        {
    //            if let jsonData = try? NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
    //            {
    //                if let jsonResult: NSArray = (try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)) as? NSArray
    //                {
    //
    //
    //                    NumberOfRows = jsonResult.count
    //                    for item in jsonResult{
    //                        country = item["Country"] as! String
    //                        capital = item["Capital"] as! String
    //
    //                        latitude = item["Latitude"] as! Double
    //
    //                        longitude = item["Longitude"] as! Double
    //
    //                        print(country)
    //
    //
    //                    }
    //                }
    //            }
    //        }
    //    }
    
    
    /**
     This function calls the user info services by sending access token to the service url
     - Parameter accessToken: access token of the user
     - Parameter url: url of user info service for getting user details
     */
    
    func callServForUserDetails(accessToken:String,url:String)
    {
        
        var firstName:String!
        var lastName:String!
        var middleName:String!
        var emailId:String!
        var university:String!
        var phoneNo:String!
        var userImage:String!
        var userType:String!
        print("access token in user details class is \(accessToken)")
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        // userDetailsClass = []
        do
        {
            print("entering into do loop")
            request.setValue("Bearer "+accessToken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                //print("Response: \(response)")
                //                let strData = NSString(data: data!, encoding: NSUTF8StringEncoding) as! AnyObject
                var strData:[String:AnyObject]
                do
                {
                    try strData = NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as AnyObject! as! [String : AnyObject]
                    print("Body: \(strData)")
                    var size = strData.count
                    print("size of response data \(size)")
                    if size != 0
                    {
                        //print("access token is \(strData["access_token"]!)")
                        firstName = strData["FirstName"]! as? String
                        emailId = strData["Email"]! as? String
                        lastName = strData["LastName"]! as? String
                        university = strData["University"]! as? String
                        phoneNo = strData["PhoneNumber"]! as? String
                        userType = strData["UserType"]! as? String
                        //                        self.editPopulation()
                        self.userClass = UserDetails(firstName: firstName, lastName: lastName, middileName: "", emailId: emailId, university: university, phoneno: phoneNo,userType: userType,status:0 )
                        self.appdelegate.userDetails = self.userClass
                        print("just checking : "+self.appdelegate.userDetails.emailId)
                        let lvc = LoginViewController()
                        // lvc.userClass = self.userClass
                        //print("Harsha yadav email id"+self.userClass.emailId)
                        //self.userDetailsClass.append(self.userClass)
                        //AppDelegate appDelegate = AppDelegate()
                        //print("class is \(self.userClass.emailId)")
                        // print(self.userDetailsClass.emailId)
                        // print("email address of user is \(strData["Email"]!)")
                        
                    }
                    else
                    {
                        print("some thing wrong with the service")
                        //callbackForUserDetails()
                    }
                    //                    dispatch_async(dispatch_get_main_queue()){
                    //                        NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                    //                    }
                    
                }
                catch
                {
                    print("soome wrong")
                    //self.callbackForUserDetails()
                }
                
                
            })
            task.resume()
            
        }
        
    }
}