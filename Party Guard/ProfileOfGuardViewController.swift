//
//  ProfileOfGuardViewController.swift
//  Party Guard
//
//  Created by Rachakunta,Naga Sri Harsha Yadav on 9/27/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the profile(update and viewing )functionality of guard user
class ProfileOfGuardViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    /// declaring variable for logout service class
    var logoutClass:LogoutService!
    /// declaring variable for app delegate class
    var appdelegate:AppDelegate!
    /// variable to hold image from user defaults
    var retrievedImg:UIImage!
    
    /// declaring variable to hold the status code
    var statusCode1:Int!
    /// declaring and initialising dictionary to hold user details
    var userDetails = Dictionary<String,String>()
    override func viewDidLoad() {
        super.viewDidLoad()
        hidingTF(true)
        addingLogoutBTN()
        buttonLayout(editProfileBTN)
         self.navigationItem.title = "Profile"
         appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        print("In profile view controller email id : "+appdelegate.userDetails.emailId)
        if appdelegate.accesstoken != nil
        {
            print("access token from app delegate is \(appdelegate.accesstoken)")
            gettingUserInfo(appdelegate.accesstoken)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileOfGuardViewController1.userinfoRes(_:)), name: "Data Delivered", object: nil)
        }        
        
        appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
             retrievedImg = UIImage(data: imgData)
        }

        GuardIV.image = retrievedImg
        
        addingImageasRightBarButton()
        
        // Do any additional setup after loading the view.
    }
    
    /**
     This function hides the text fields
     - Parameter value: holds the boolean from function call
     */
    func hidingTF(value:Bool)
    {
        lNameTF.hidden = value
        fnameTF.hidden = value
        mNameTF.hidden = value
        emailAddTF.hidden = value
        phNOTF.hidden = value
        universityTF.hidden = value
        
    }
    ///Last name text field
    @IBOutlet weak var lNameTF: UITextField!
    ///middle name text field
    @IBOutlet weak var mNameTF: UITextField!
    ///first name text field
    @IBOutlet weak var fnameTF: UITextField!
    ///las name label
    @IBOutlet weak var lNameLBL: UILabel!
    ///middle name label
    @IBOutlet weak var mNameLBL: UILabel!
    ///first name label
    @IBOutlet weak var fNameLBL: UILabel!
    
    ///email address label
    @IBOutlet weak var emailAddLBL: UILabel!
    ///phonw number text field
    @IBOutlet weak var phNOTF: UITextField!
    ///email id text field
    @IBOutlet weak var emailAddTF: UITextField!
    ///phone number label
    @IBOutlet weak var phNoLBL: UILabel!
    
    ///university text field
    @IBOutlet weak var universityTF: UITextField!
    ///university lable
    @IBOutlet weak var universityLBL: UILabel!    
    ///guard Image view
    @IBOutlet weak var GuardIV: UIImageView!
    ///edit profile button
    @IBOutlet weak var editProfileBTN: UIButton!
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        self.navigationItem.title = "Profile"
        //addSlideMenuButton()
    }
    
    /**
     This function displays the labels after response from the service
     - Parameter notification: notification from NSNotification
     */
    func userinfoRes(notification:NSNotification)
    {
        showingDataForLabels()
    }  
    
    /**
     This function adds the button at top right corner in navigation item
     */
    
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(retrievedImg, forState: UIControlState.Normal)
        //add function for button
        button.addTarget(self, action: Selector("imageButtonPressed"), forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }

    /**
     This function is an action performs validation and calls the edit service when user clicks on edit button
     - Parameter sender: sender of object-  button
     */
    @IBAction func editProfileAction(sender: AnyObject) {
        
        print("printing edit profile actionp \(fnameTF.text!)")
        
        //editProfileBTN.addTarget(self, action: #selector(ProfileOfGuardViewController1.Save(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        if editProfileBTN.titleLabel?.text == "Save"
        {
            print("In save")
            print(fnameTF.text!)
            if fnameTF.text == "" || fnameTF.text == nil
            {
                
                fnameTF.attributedPlaceholder = NSAttributedString(string: "Enter First Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if lNameTF.text == "" || lNameTF.text == nil
            {
                
                lNameTF.attributedPlaceholder = NSAttributedString(string: "Enter Last Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if emailAddTF.text == "" || emailAddTF.text == nil
            {
                
                emailAddTF.attributedPlaceholder = NSAttributedString(string: "Enter Eamil Id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if phNOTF.text == "" || phNOTF.text == nil
            {
                
                phNOTF.attributedPlaceholder = NSAttributedString(string: "Enter Phone number", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if universityTF.text == "" || universityTF.text == nil
            {
                
                universityTF.attributedPlaceholder = NSAttributedString(string: "Enter University ", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
            }
            else if emailAddTF.text != ""
            {
                
                let emailid:String = emailAddTF.text!
                let statusOfValidation:Bool = validateEmail(emailid)
                if statusOfValidation == true
                {
                    print("firstName in save action is \(fnameTF.text!)")
                    callingUpdateProfileService(["firstName":fnameTF.text!,"lastName":lNameTF.text!,"PhoneNumber":phNOTF.text!,"imgUrl":"http://helloimage.com/image1","age":"34"], url: appdelegate.servicesDomainUrl+"api/Account/UpdateProfile")
                    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileOfGuardViewController1.editProfileResponse(_:)), name: "Data Delivered", object: nil)
                }
                else
                {
                    
                    print("entered email is not correct")
                    displayAlertController("Input Alert", message: "Please enter valid email")
                }
            }
            
        }
        else
        {
            editProfileBTN.setTitle("Save", forState: UIControlState.Normal)
            
            hidingTF(false)
            universityTF.hidden = true
            self.editPopulation()
        }
        

        
    }
    
    /**
     This function adjusts the button layout
     - Parameter butotName: button in the view controller
     */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }
    
    /// This function displays the values for labels
    func showingDataForLabels()
    {
        universityLBL.hidden = true
        //print(" lastname label is \(userDetails["LastName"]!)")
        lNameLBL.text = appdelegate.userDetails.lastName
        fNameLBL.text = appdelegate.userDetails.firstName
        mNameLBL.text = ""
        phNoLBL.text = appdelegate.userDetails.phoneNo
        emailAddLBL.text = appdelegate.userDetails.emailId
        universityLBL.text = appdelegate.userDetails.university
        universityLBL.hidden = true
        
    }

    
    ///adding logout button as bar button item
    
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }
    
    /**
     This function saves the details in the service after editing the details 
     - Parameter sender: sender of button
     */
    func Save(sender: UIButton) {
        var flag:Bool = true
        print(fnameTF.text!)
        if fnameTF.text == "" || fnameTF.text == nil
        {
            flag = false
            fnameTF.attributedPlaceholder = NSAttributedString(string: "Enter First Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if lNameTF.text == "" || lNameTF.text == nil
        {
            flag = false
            lNameTF.attributedPlaceholder = NSAttributedString(string: "Enter Last Name", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if emailAddTF.text == "" || emailAddTF.text == nil
        {
            flag = false
            emailAddTF.attributedPlaceholder = NSAttributedString(string: "Enter Eamil Id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if phNOTF.text == "" || phNOTF.text == nil
        {
            flag = false
            phNOTF.attributedPlaceholder = NSAttributedString(string: "Enter Phone number", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if universityTF.text == "" || universityTF.text == nil
        {
            flag = false
            universityTF.attributedPlaceholder = NSAttributedString(string: "Enter University ", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if emailAddTF.text != ""
        {
            flag = false
            let emailid:String = emailAddTF.text!
            let statusOfValidation:Bool = validateEmail(emailid)
            if statusOfValidation == true
            {
                print("firstName in save action is \(fnameTF.text!)")
                callingUpdateProfileService(["firstName":fnameTF.text!,"lastName":lNameTF.text!,"PhoneNumber":phNOTF.text!,"imgUrl":"http://helloimage.com/image1","age":"34"], url: appdelegate.servicesDomainUrl+"api/Account/UpdateProfile")
                NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileOfGuardViewController1.editProfileResponse(_:)), name: "Data Delivered", object: nil)
            }
            else
            {
                flag = false
                print("entered email is not correct")
                displayAlertController("Input Alert", message: "Please enter valid email")
            }
        }
        
        
    }
    
    /**
     function validating the email entered by user
     _ Parameter enteredEmail: user entered email id
     */
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(enteredEmail)
        
    }
    
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertController(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    
    func displayAlertController1(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("HomeVC") as! SelectFraternityViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    /**
     This function calls the user info service and retrieves the response
     
     - Parameter  acdessToken: access token of user
     
     */
    
    func gettingUserInfo(accessToken:String!)
    {
        //var userDetails1 = Dictionary<String,String>()
        //print("access token is \(accessToken)")
        let request = NSMutableURLRequest(URL: NSURL(string: appdelegate.servicesDomainUrl+"api/Account/UserInfo")!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        
        do
        {
            print("do loop")
            request.setValue("Bearer "+accessToken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                //print("Response: \(response)")
                //                let strData = NSString(data: data!, encoding: NSUTF8StringEncoding) as! AnyObject
                var strData:[String:AnyObject]
                do
                {
                    try strData = NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as AnyObject! as! [String : AnyObject]
                    //print("Body: \(strData)")
                    let size = strData.count
                    //print("size of response data \(size)")
                    if size != 0
                    {
                        //print("access token is \(strData["access_token"]!)")
                        self.appdelegate.userDetails.firstName = strData["FirstName"]! as? String
                        self.appdelegate.userDetails.emailId = strData["Email"]! as? String
                        self.appdelegate.userDetails.lastName = strData["LastName"]! as? String
                        //self.appdelegate.userDetails.university = strData["University"]! as? String
                        self.appdelegate.userDetails.phoneNo = strData["PhoneNumber"]! as? String
                        print("phone no in access token method is \(self.appdelegate.userDetails.phoneNo)")
                        //                        self.editPopulation()
                        
                        
                        //print("userName is \(strData["Email"]!)")
                        
                    }
                    else
                    {
                        self.callbackForProfile()
                        //self.dataResponse = 1
                    }
                    
                    dispatch_async(dispatch_get_main_queue()){
                        NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                    }
                    
                }
                catch
                {
                    print("soome wrong")
                }
                
                
            }).resume()
            
        }
        
    }
    
    /// call back function for profile
    
    func callbackForProfile()
    {
        
        if let path = NSBundle.mainBundle().pathForResource("profile", ofType: "json")
        {
            if let jsonData = try? NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
            {
                if let jsonResult:NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)) as? NSDictionary
                {
                    self.userDetails["Email"] = jsonResult["Email"] as? String
                    //print("email  is \(jsonResult["Email"]!)")
                    
                    self.userDetails["FirstName"] = jsonResult["FirstName"] as? String
                    //print(self.userDetails["FirstName"])
                    self.userDetails["LastName"] = jsonResult["LastName"] as? String
                    self.userDetails["University"] = jsonResult["University"] as! String
                    self.userDetails["PhoneNumber"] = jsonResult["PhoneNumber"] as! String
                    //var age = item["Age"] as! String
                    
                }
            }
        }
        
        
    }


    /// function to populate the textfields
    func editPopulation()
    {
        print("first name in edit population is \(appdelegate.userDetails.firstName)")
        lNameTF.text = appdelegate.userDetails.lastName
        fnameTF.text = appdelegate.userDetails.firstName
        mNameTF.text = ""
        phNOTF.text = appdelegate.userDetails.phoneNo
        emailAddTF.text = appdelegate.userDetails.emailId
        universityTF.text = appdelegate.userDetails.university
    }

    
    /**
     This function updates the profile by calling update profile service
     
     - Parameter  params: dictinary holds the user details as key value pairs
     - Parameter  url: url of update profile service
     */
    func callingUpdateProfileService(params : Dictionary<String, String>, url : String)
    {
        print("parameters in calling update profile service \(params.debugDescription)")
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.setValue("bearer "+appdelegate.accesstoken, forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            //print("request is \(request)")
            //print("request body is \(request.debugDescription)")
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                //print("Response: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                //print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
                
            }).resume()
            
        }
        catch
        {
            print("something is wrong")
            
        }
        
    }

    // implements logout functionality
    
    func Logout_Functionality()
    {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
        //callingLogoutService()
        
    }
    
    /// function calls the logout service
    func callingLogoutService()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "LogoutResponse:", name: "Data Delivered", object: nil)
        logoutClass = LogoutService()
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
        //        statusCode = logoutClass.statusCode1
        //        print("code is \(statusCode)")
    }
    
    
    /**
     This function alerts the user based on service response
     
     - Parameter  notification: NSNotification
   
     */
    func LogoutResponse(notification: NSNotification)
    {
        //print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
            displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }
    
    
    /**
     This function alerts the user based on service response
     
     - Parameter  notification: NSNotification
     
     */
    func editProfileResponse(notification: NSNotification)
    {
        if statusCode1 == 200
        {
            displayAlertController("Success Alert", message: "Your details has been saved")
            hidingTF(true)
            appdelegate.userDetails.lastName = lNameTF.text!
            appdelegate.userDetails.firstName = fnameTF.text!
            appdelegate.userDetails.phoneNo = phNOTF.text!
            print("phone number is \(appdelegate.userDetails.phoneNo)")
            appdelegate.userDetails.university = universityTF.text!
            showingDataForLabels()
            editProfileBTN.setTitle("Edit", forState: UIControlState.Normal)
            editProfileBTN.addTarget(self, action: #selector(ProfileOfGuardViewController.editProfileAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
        }
        else
        {
            displayAlertController("Profile Alert", message: "Please check your details")
        }
    }

    
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //Read the image picked from UIImagePickerController
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        print("Image Selected")
        self.dismissViewControllerAnimated(true, completion: nil)
        GuardIV.image = image
        let data = UIImagePNGRepresentation(image)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "myImageKey")
        NSUserDefaults.standardUserDefaults().synchronize()

    }

    /**
     This function is an action to import image from app
     
     - Parameter  sender: sender of object - button
    
     */
    @IBAction func importImage(sender: UIButton) {
        let myActionSheet = UIAlertController(title: "Select image", message: "Where would you add images from?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        // choose from photos action button
        let chooseAction = UIAlertAction(title: "Choose from photos", style: UIAlertActionStyle.Default) { (action) in
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            image.allowsEditing = false
            self.presentViewController(image, animated: true, completion: nil)
        }
        //photo action button
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.Default) { (action) in
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerControllerSourceType.Camera
            image.allowsEditing = false
            self.presentViewController(image, animated: true, completion: nil)
        }
        
        // cancel action button
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action) in
            print("Cancel action button tapped")
        }
        // add action buttons to action sheet
        myActionSheet.addAction(chooseAction)
        myActionSheet.addAction(takePhotoAction)
        myActionSheet.addAction(cancelAction)
        // support iPads (popover view)
        if let popoverController = myActionSheet.popoverPresentationController {
            popoverController.sourceView = sender as! UIView
            popoverController.sourceRect = sender.bounds
        }
        //myActionSheet.popoverPresentationController!.sourceView = self.
        //        myActionSheet.popoverPresentationController!.sourceRect = CGRectMake(self.addImageBTN.bounds.size.width / 2.0, self.addImageBTN.bounds.size.height / 2.0, 1.0, 1.0)
        
        //        myActionSheet.popoverPresentationController?.sourceView = self.view
        //        myActionSheet.popoverPresentationController?.sourceRect = self.view.bounds
        // present the action sheet
        self.presentViewController(myActionSheet, animated: true, completion: nil)
    }    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
