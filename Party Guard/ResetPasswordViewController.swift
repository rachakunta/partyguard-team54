//
//  ResetPasswordViewController.swift
//  Party Guard
//
//  Created by Harish K on 9/3/16.
//  Copyright (c) 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the reset password functionality for basic user
class ResetPasswordViewController: UIViewController {

    /// reset button object
    @IBOutlet weak var resetBtn: UIButton!
    /// appdelegate object
    var appdelegate:AppDelegate!
    ///status code object
    var statusCode1:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
          appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        // Do any additional setup after loading the view.
        buttonLayout(resetBtn)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "resetResponse:", name: "Data Delivered", object: nil)
       
    }

    /// confirm password text field
    @IBOutlet weak var confirmPasswordTF: UITextField!
    /// Password  text field
    @IBOutlet weak var passwordTF: UITextField!
    /// email id  text field
    @IBOutlet weak var emaildIdTF: UITextField!
    /** function for button layout
     - Parameter buttonName:UIButton
    */
    func buttonLayout(buttonName:UIButton)
    {
        buttonName.layer.cornerRadius = 10
        buttonName.layer.borderWidth = 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /** function for reset password fields
     - Parameter sender: AnyObject
     */
    @IBAction func reset(sender: AnyObject) {
       
        
        if emaildIdTF.text == "" || emaildIdTF.text == nil
        {
            //displayAlertController("Input Alert", message: "Please enter your emailId")
            emaildIdTF.attributedPlaceholder = NSAttributedString(string: "Enter Email Id", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if passwordTF.text == "" || passwordTF.text == nil
        {
            //displayAlertController("Input Alert", message: "Please enter your new password")
            passwordTF.attributedPlaceholder = NSAttributedString(string: "Enter New Password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if confirmPasswordTF.text == "" || confirmPasswordTF.text == nil
        {
            //displayAlertController("Input Alert", message: "Please confirm your password")
             confirmPasswordTF.attributedPlaceholder = NSAttributedString(string: "Confirm New Password", attributes: [NSForegroundColorAttributeName: UIColor.redColor()])
        }
        else if emaildIdTF.text != ""
        {
            let emailid:String = emaildIdTF.text!
            let statusOfValidation:Bool = validateEmail(emailid)
            if statusOfValidation == true
            {
                let password:String = passwordTF.text!
                let confirmPassword:String = confirmPasswordTF.text!
                print("entered email id correct")
                
                print(password)
                print(confirmPassword)
                let statusOfpasswords = checkPasswords(password, confirmPwd: confirmPassword)
                if statusOfpasswords == true
                {
                    let passwordurl = appdelegate.servicesDomainUrl+"api/Account/NewPasswordSet"
                    print("password url is \(passwordurl)")
                     callserviceForPasswordReset(["Email":emaildIdTF.text!,"NewPassword":passwordTF.text!,"ConfirmPassword":confirmPasswordTF.text!], url: passwordurl )
                    
                    
                }
                else{
                    displayAlertController("Input Alert", message: "Passwords did not match")
                }
            }
            else
            {
                print("entered email is not correct")
                displayAlertController("Input Alert", message: "Please enter valid email")
            }

        }
        
       self.navigationController?.popToRootViewControllerAnimated(true)
    
    }
    /**
     This function validates weather the password and confirm password values are same
     - Parameter password: String entered in password
     - Parameter confirmPwd: String entered in confirm password feild.
     */
    func checkPasswords(password:String, confirmPwd:String)->Bool
    {
        if password ==  confirmPwd
        {
             return true
        }
        else{
            return false
        }
       
    }
    /**
     This function calls the password reset service and parses the response
     - Parameter params: dictionary with parameter names as keys and values of emaild, password and grant type
     - Parameter url: url of registration service url
     */
    
    func callserviceForPasswordReset(params : Dictionary<String, String>,url:String)
    {
        print(params.debugDescription)
        print("post method")
        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        //session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        //var task
        var err: NSError?
        do
        {
            print("do loop")
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print("request is \(request)")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Body: \(strData)")
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                self.statusCode1 = httpResponse?.statusCode
                
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
                
            }).resume()
            
        }
        catch
        {
            print("something is wrong")
            //callbackforRegister()
        }

    }
    /**
     This function will send confirmation dailog box whether the password is reset or no
     - Parameter notification: confirms if the password is resot or no
     */
    func resetResponse(notification: NSNotification)
    {
        print("status code is \(statusCode1!)")
        if statusCode1! == 200
        {
            print("status in if loop is \(statusCode1!)")
            displayAlertController1("Reset Alert", message: "Your password has been reset")
        }
        else
        {
            displayAlertController1("Reset Alert", message: "Some thing has gone wrong with the server")
        }
    }
    /**
     This function will validate the email id entered
     - Parameter enteredEmail:String
     */
    
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluateWithObject(enteredEmail)
        
    }
    /**
     This function redirects the screen to login View controller
     - Parameter title:String this is the title of the alert box
     - Parameter message:String this is the message to display as an alert
     */
    func displayAlertController(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in}))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    /**
     This function redirects the screen to login View controller
     - Parameter title:String this is the title of the alert box
     - Parameter message:String this is the message to display as an alert
     */
    func displayAlertController1(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

   
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
