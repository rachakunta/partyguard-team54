//
//  PartyGuardTeamViewController.swift
//  Party Guard
//
//  Created by Rachakunta,Naga Sri Harsha Yadav on 9/27/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the party gaurd team functionality of viewing guards of fraternity.
class PartyGuardTeamViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate  {
    /// grid view object
    @IBOutlet weak var gridGuardView: UICollectionView!
    /// appdelegate object
    var appdelegate:AppDelegate!
    /// logout service object
    var logoutClass:LogoutService!
    /// flag object to evaluate the status of the guard
    var switchStat:Bool!
    /// guard class object
    var guardClass:Guard!
    /// status code varible
    var statusCode1:Int!
    /// selected grid variable
    var selectedGrid:Int!
    /// array of guards
    var guardArray:[Guard] = []
    /// image object
    var retrievedImg:UIImage!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
         appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:1,left:10,bottom:10,right:10)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        switchStat = false
        gridGuardView.collectionViewLayout = layout
        callPGTeamService(appdelegate.accesstoken, url: self.appdelegate.servicesDomainUrl+"api/HostUserProfile/DisplayGuardDetails")
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PartyGuardTeamViewController.pgTeamResponse(_:)), name: "Data Delivered", object: nil)
        addingLogoutBTN()
        
        if let imgData = NSUserDefaults.standardUserDefaults().objectForKey("myImageKey") as? NSData {
            retrievedImg = UIImage(data: imgData)
        }
        addingImageasRightBarButton()
        // Do any additional setup after loading the view.
    }
    
    ///This method adds the button with background as user image
    func addingImageasRightBarButton()
    {
        let button: UIButton = UIButton(type: .Custom)
        //set image for button
        button.setImage(retrievedImg, forState: UIControlState.Normal)
        //add function for button
        button.addTarget(self, action: "imageButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        //set frame
        button.frame = CGRectMake(0, 0, 53, 31)
        
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    /**
     This function reloads the collection view after the service response
     - Parameter notification: notification from NSNotification
     */
    func pgTeamResponse(notification:NSNotification)
    {
        gridGuardView.reloadData()
    }
    
    /**
     This function calls PGTeam web service using post request by sending accesstoken to url
     - Parameter accessToken: access token of user
     - Parameter url: PGTeam service url
     */
    func callPGTeamService(accessToken:String,url:String)
    {
        var guardProfileID:Int!
        var firstName:String!
        var email:String!
        var lastName:String!
        var phoneNumber:String!
        var isActive:Bool!
        var age:String!
        var guardUserCode:String!
        var guardEmail:String!
        var imgURL:String!
        print("access token in user details class is \(accessToken)")
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        // userDetailsClass = []
        do
        {
            print("entering into do loop")
            request.setValue("Bearer "+accessToken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                //print("Response: \(response)")
                //                let strData = NSString(data: data!, encoding: NSUTF8StringEncoding) as! AnyObject
                var strData:[String:AnyObject]
                do
                {
                    let strData = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! [AnyObject]
                    print("Body: \(strData)")
                    var size = strData.count
                    print("size of response data \(size)")
                    
                    if size != 0
                    {
                        for eachItem in strData
                        {
                        //print("access token is \(strData["access_token"]!)")
                        firstName = eachItem["firstName"]! as? String
                        lastName = eachItem["lastName"]! as? String
                        age = eachItem["age"]! as? String
                        isActive = eachItem["isActive"]! as? Bool
                        guardUserCode = eachItem["guardUserCode"]! as? String
                        guardProfileID = eachItem["guardProfileID"] as? Int
                        let applicationUser = eachItem["ApplicationUser"]
                        email = applicationUser!!["Email"]! as? String
                        phoneNumber = applicationUser!!["PhoneNumber"]! as? String
                        
                        //guard
//                        let httpResponse = response as? NSHTTPURLResponse
//                        print("response code is \(httpResponse?.statusCode)")
//                        self.statusCode1 = httpResponse?.statusCode
//                        //                        self.editPopulation()
                        self.guardClass = Guard(firstName: firstName, lastName: lastName, email: email, phoneNo: phoneNumber, profileID: guardProfileID, imgURL: " ")
                            self.appdelegate.guardclass = self.guardClass
                            self.guardArray.append(self.guardClass)
                       
                        }
                        
                    }
                    else
                    {
                        print("some thing wrong with the service")
                        //callbackForUserDetails()
                    }
                    //                    dispatch_async(dispatch_get_main_queue()){
                    //                        NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                    //                    }
                    
                }
                catch
                {
                    print("soome wrong")
                    //self.callbackForUserDetails()
                }
                
                
            })
            task.resume()
            
        }
        

    }
    override func viewWillAppear(animated: Bool) {
        print("selected value \(switchStat)")
        addingLogoutBTN()
        gridGuardView.reloadData()
    }
    override func viewDidAppear(animated: Bool) {
        self.navigationItem.title = "Party Guard Team"
        print("-----------------------------------------------")
    }
    
    /// adding logout button to navigation item at top right corner
    func addingLogoutBTN()
    {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(Logout_Functionality))
    }
    

    /// implements the logout functionality
    func Logout_Functionality()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
        self.presentViewController(nextViewController, animated: true, completion: nil)
        //callingLogoutService()
    }
    
    /// this function calls the logout service for logout functionality
    func callingLogoutService()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "LogoutResponse:", name: "Data Delivered", object: nil)
        logoutClass = LogoutService()
        logoutClass.callService(appdelegate.accesstoken,url:appdelegate.servicesDomainUrl+"api/Account/Logout")
        //        statusCode = logoutClass.statusCode1
        //        print("code is \(statusCode)")
    }
    
    /**
     This function displays alerts to user based on response from service
     -Parameter notification: notificaiton from NSNotification
     */
    func LogoutResponse(notification: NSNotification)
    {
        print("status code is \(logoutClass.statusCode1!)")
        if logoutClass.statusCode1! == 200
        {
            print("successful logout")
            displayAlertForLogout("Logout alert", message: "Successful logoout")
            
        }
        else
        {
            displayAlertForLogout("Logout alert", message: "Some problem in logout")
        }
    }
    
    /**
     This function displays the alerts with a message based on user action and redirects to login screen
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertForLogout(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("UserloginVC") as! LoginViewController
            self.presentViewController(nextViewController, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return guardArray.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GuardCollectionCell", forIndexPath: indexPath) as! GuardViewCell
        let bcolor : UIColor = UIColor( red: 0.2, green: 0.2, blue:0.2, alpha: 0.3 )
        cell.layer.borderColor = bcolor.CGColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 7
        
        
         cell.backgroundColor=UIColor.grayColor()
        if switchStat == true
        {
            cell.ColorV.backgroundColor = UIColor.greenColor()
        }
        else
        {
            cell.ColorV.backgroundColor = UIColor.redColor()
        }
        cell.ColorV.backgroundColor = UIColor.redColor()
        cell.ColorV.layer.cornerRadius = cell.ColorV.frame.width/2
        cell.ColorV.layer.masksToBounds = true
        cell.GuardImageIV.image = UIImage(named: "Profile.png")
//        cell.lblCell.text = tableData[indexPath.row]
//        cell.imgCell.image = UIImage(named: tableImages[indexPath.row])
        return cell
    }
    
    
//    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        
//        
//        selectedGrid = indexPath.row
//        print("grid is \(selectedGrid)")
//    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSize(width: 105  , height: 85)
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let indexPath = gridGuardView?.indexPathForCell(sender as! UICollectionViewCell)
        {
            if (segue.identifier == "guardSegue") {
            
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destinationViewController as! GuardDetailsViewController
            // your new view controller should have property that will store passed value
            //NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
            //print("Selected Row is \(self.tableView.indexPathForSelectedRow)")
            print("selected grid is \(selectedGrid)")
            viewController.selectedGuard = guardArray[indexPath.row]
        }
        }
    }

    
    
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
