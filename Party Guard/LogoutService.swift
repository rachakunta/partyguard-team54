//
//  LogoutService.swift
//  Party Guard
//
//  Created by Harish K on 10/29/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import Foundation
/// This class implements the logout service functionality
class LogoutService
{
    /// this variable holds the status code of service
    var statusCode1:Int!
    
   
    /**
     This function calls the logout services
     - Parameter accessToken: access token of user 
     - Parameter url: url of logout service
 */
    func callService(accessToken:String,url:String)
    {
       
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        print("request is \(request.description)")
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        // do loop for detecting exception if any
        do
        {
            print("do loop")
            // adding access token and authorization field in the request
            request.setValue("Bearer "+accessToken, forHTTPHeaderField: "Authorization")
            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                print("Response: \(response)")
                // casting the web service response as httpresponse
                let httpResponse = response as? NSHTTPURLResponse
                print("response code is \(httpResponse?.statusCode)")
                // assigning service status code in temporary variable
                self.statusCode1 = httpResponse?.statusCode
                print("status code in logout class is \(self.statusCode1)")
                
                // for runnin on the main thread
                dispatch_async(dispatch_get_main_queue()){
                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
                }
                
            }).resume()
        }
        catch
        {
            print("something is wrong")
        }

    }
}
