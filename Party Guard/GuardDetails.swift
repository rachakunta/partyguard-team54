//
//  GuardDetails.swift
//  Party Guard
//
//  Created by Harish K on 10/21/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import Foundation
/// This class holds the guard details
class GuardDetails
{
    /// outlet for guardProfileID textfield
    var guardProfileID:Int!
    /// outlet for firstName textfield
    var firstName:String!
    /// outlet for email textfield
    var email:String!
    /// outlet for lastName textfield
    var lastName:String!
    /// outlet for phoneNumber textfield
    var phoneNumber:String!
    /// outlet for isActive textfield
    var isActive:String!
    /// outlet for age textfield
    var age:String!
    /// outlet for guardUserCode textfield
    var guardUserCode:String!
    /// outlet for guardEmail textfield
    var guardEmail:String!
    /// outlet for guardImage textfield
    var guardImage:String!
   
    /**
     This is a default constructor
     */
    init()
    {}
    
    /**
     This is a parameterized constructor
     - Parameter firstName first name
     - Parameter email email id
     - Parameter lastName last name
     - Parameter phoneNumber phone number
     - Parameter isActive variable to know whether active or not
     - Parameter age guard age
     - Parameter guardUserCode guard code
     - Parameter guardEmail guard email
     - Parameter guardProfileID guard profile id
     - Parameter guardImage guard image
     */
    init(firstName:String, email:String, lastName:String, phoneNumber:String, isActive:String,age:String,guardUserCode:String,guardEmail:String,guardProfileID:Int,guardImage:String){
        self.firstName = firstName
        self.email = email
       self.lastName = lastName
        self.phoneNumber = phoneNumber
        self.isActive = isActive
        self.guardUserCode = guardUserCode
        self.guardEmail = guardEmail
        self.guardProfileID = guardProfileID
        self.guardImage = guardImage
    }
    
}
