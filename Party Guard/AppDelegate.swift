//
//  AppDelegate.swift
//  Party Guard
//
//  Created by Kola,Harish on 7/19/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var role:Int!
    var accesstoken:String!
    var servicesDomainUrl:String!
    var userDetails = UserDetails()
    var guardDetails = GuardDetails()
    var guardclass = Guard()
   
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        servicesDomainUrl = "http://partyguardservices20161110094537.azurewebsites.net/"
        
        let notificationSettings = UIUserNotificationSettings(forTypes: [.Badge, .Alert, .Sound], categories: nil)
        
        UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
        //self.createLocalNotification()
        
        return true
        
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    
    
    
    
    
    //This is the code to generate local notifications
    
    
    
    
    
    func createLocalNotification()
    {
        let localNotification = UILocalNotification()
        
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 3)
        
        localNotification.soundName = UILocalNotificationDefaultSoundName
        
        localNotification.userInfo = [
            "message" : "Check out our app buddy 1"
        ]
        localNotification.alertBody = "Check buddy 2"
        
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
    }
    
    // 5
    // when the user clicks on the noti, this method gets called
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        if application.applicationState == .Active{
            
        }
        self.takeActionWithNotification(notification)
        
    }
    
    // 6
    func takeActionWithNotification(localNotification: UILocalNotification)
    {
        let notificationMessage = localNotification.userInfo!["message"] as! String
        let userName = "Rachakunta Harsha"
        
        let alertController = UIAlertController(title: "Hey \(userName)", message: notificationMessage, preferredStyle: .Alert)
        
        let remindMeLaterAction = UIAlertAction(title: "Remind Me Later", style: .Default, handler: nil)
        
        let sureAction = UIAlertAction(title: "Sure", style: .Default) { (action) -> Void in
//            let tabBarController = self.window?.rootViewController as! UITabBarController
//            tabBarController.selectedIndex = 0
            
        }
        alertController.addAction(remindMeLaterAction)
        alertController.addAction(sureAction)
        
        self.window?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
    }

    
    
    
    
    
    //This ends the local notification code

}

