//
//  Guard.swift
//  Party Guard
//
//  Created by Kola,Harish on 11/16/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import Foundation
/// This class is used to store the details of guard user
class Guard
{
    /// outlet for firstName textfield
    var firstName:String!
    /// outlet for lastName textfield
    var lastName:String!
    /// outlet for email textfield
    var email:String!
    /// outlet for phoneNo textfield
    var phoneNo:String!
    /// outlet for profileID textfield
    var profileID:Int!
    /// outlet for imgURL textfield
    var imgURL:String!
    
    /**
     This is a default constructor
     */
    
    init()
    {}
    
    
    /**
     This is a parameterized constructor
     - Parameter firstName first name
     - Parameter lastName last name
     - Parameter email email
     - Parameter phoneNo phone number
     - Parameter profileID profile id
     - Parameter imgURL image URL
     
     */
    init(firstName:String,lastName:String,email:String,phoneNo:String,profileID:Int,imgURL:String)
    {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phoneNo = phoneNo
        self.profileID = profileID
        self.imgURL = imgURL
    }
}
