//
//  GuardDetailsViewController.swift
//  Party Guard
//
//  Created by Harish K on 10/21/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the view functionality of guard details
class GuardDetailsViewController: UIViewController {

    /// guard image view
    @IBOutlet weak var guardImageIV: UIImageView!
    
    /// first name label
    @IBOutlet weak var firstnameLBL: UILabel!
    
    
    /// last name label
    @IBOutlet weak var lastnameLBL: UILabel!
    
    /// phone number label 
    @IBOutlet weak var phonenumberLBL: UILabel!
    
    /// email id label 
    @IBOutlet weak var emailidLBL: UILabel!
    /// selected guard object
    var selectedGuard:Guard!
    /// apdelegate object
    var appdelegate:AppDelegate!
    /// status of guard switch variable
    var switchStatus:Bool = false
   
    
    /// Party guard team view controller object
    var partyGuardVC:PartyGuardTeamViewController!
    
    /// Uiswitch outlet
    @IBOutlet weak var avaliabilitySW: UISwitch!
    var guardRequests:[Guard] = []
    //var guard1 = GuardDetails(firstName: "Richard", email: "richard@gmail.com", lastName: "Sally", phoneNumber: "660 5336789", isActive: "Yes",age: "23",guardUserCode: "56676767",guardEmail: "harish@gmail.com",guardProfileID: 34,guardImage: "http://url1.gmails.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustPictureView()
        
        guardRequests.append(selectedGuard)
        //callbackForGuardDetails()
        guardImageIV.image = UIImage(named: "GuardImage")
        firstnameLBL.text = selectedGuard.firstName
        lastnameLBL.text = selectedGuard.lastName
        emailidLBL.text = selectedGuard.email
        phonenumberLBL.text = selectedGuard.phoneNo
        
        print("status of aval \(avaliabilitySW.selected)")
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(animated: Bool) {
//        if avaliabilitySW.on
//        {
//            partyGuardVC.switchStat = true
//        }
//        else
//        {
//            partyGuardVC.switchStat = false
//        }
    }
    /// method to adjust image view
    func adjustPictureView()
    {
        guardImageIV.layer.borderWidth = 1.0
        guardImageIV.layer.masksToBounds = false
        guardImageIV.layer.borderColor = UIColor.blackColor().CGColor
        guardImageIV.layer.cornerRadius = guardImageIV.frame.size.width/2
        guardImageIV.layer.cornerRadius = guardImageIV.frame.size.height/2
        guardImageIV.clipsToBounds = true
    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// callback function for guard details
    func callbackForGuardDetails()
    {
        if let path = NSBundle.mainBundle().pathForResource("ListOfGuards", ofType: "json")
        {
            if let jsonData = try? NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
            {
                if let jsonResult: NSArray = (try? NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers)) as? NSArray
                {
                    
                    var universityArray:[String] = []
                    
                    var numberOfRows = jsonResult.count
                    print("size is \(numberOfRows)")
                    for item in jsonResult{
                        var id = item["ID"] as! String
                        var universityName = item["UniversityName"] as! String
                        
                        universityArray.append(universityName)
                    }
//                    list = universityArray
//                    print("list size is \(list.count)")
//                    dropDown.reloadAllComponents()
                }
            }
        }

    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
