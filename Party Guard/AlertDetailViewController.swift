//
//  AlertDetailViewController.swift
//  Party Guard
//
//  Created by Harish K on 10/6/16.
//  Copyright © 2016 Kola,Harish. All rights reserved.
//

import UIKit
/// This view controller implements the viewing the details of each alert for guard user
class AlertDetailViewController: UIViewController {
    
    /// declaring variable for alert details
    var alertDetails:Alert = Alert()
    
    /// declaring variable for status code
    var statusCode1:Int!
    
    /// declaring variable for  resolve status code
    var resolveStatusCode:Int!
    
    /// outlets for requester Name
    @IBOutlet weak var requesterName: UILabel!
    
    /// outlets for  incident location
    @IBOutlet weak var incidentLocation: UILabel!
    
    //@IBOutlet weak var nameLBL: UILabel!
    
    /// declaring variable for App delegate
    var appdelegate:AppDelegate!
    
     /// outlets for fraternity name
    @IBOutlet weak var fraternityName: UILabel!
    
     /// outlets for age
    @IBOutlet weak var ageLBL: UILabel!
    
     /// outlets for alert type
    @IBOutlet weak var alertType: UILabel!
    //@IBOutlet weak var incidentLocationLBL: UILabel!
    
     /// outlets for comments
    @IBOutlet weak var commentsLBL: UILabel!
    
     /// outlets for claim button
    @IBOutlet weak var claimBTN: UIButton!
    
     /// outlets for resolve button
    @IBOutlet weak var resolveBTN: UIButton!
    
    
    override func viewDidLoad() {
         appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        print("Racha Harsha")
        //print(alertDetails.comments)
        super.viewDidLoad()
        requesterName.text = alertDetails.basicUserFirstName + " " + alertDetails.basicUserLastName
        incidentLocation.text = alertDetails.issueLocation
        fraternityName.text = alertDetails.fraternityName
       //ageLBL.text = alertDetails.
        alertType.text = alertDetails.issueName
        commentsLBL.text = alertDetails.comments
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    /**
     This function is an action for claiming the request
     - Parameter sender: sender of object - button
     */
    @IBAction func claimRequest(sender: AnyObject) {
        print("issue id of alerDVC ]\(alertDetails.issueID)")
        
        serviceForClaimRequest(["id":alertDetails.issueID], url: appdelegate.servicesDomainUrl+"GuardAlertsClaim")
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AlertDetailViewController.claimResponse(_:)), name: "Data Delivered", object: nil)
        
        
        
        
    }
    
    
    /**
     This function is used for displaying the confirmation message for request is claimed or no
     - Parameter notification: sends notification for alert claimed
     */
//    func claimResponse(notification: NSNotification){
//        //print(statusCode1)
//      
//    }
//    
    
    /**
     This function is an action for resolving the issue.
     - Parameter sender: sender of object - button
     */
    @IBAction func ResolveAction(sender: AnyObject) {
        print("issue id of alerDVC ]\(alertDetails.issueID)")
        
        serviceForResolveRequest(["id":alertDetails.issueID], url: appdelegate.servicesDomainUrl+"GuardAlertsResolve")
        

        
    }
    
    /**
     This function will get the Resolve request service
     - Parameter params: parameters for resolve request service
     - Parameter url: url for resolve request service
     */
    func serviceForResolveRequest(params:Dictionary<String,Int>,url:String)
    {
        do{
            
            let jsonData = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            
            let url = NSURL(string: "http://partyguardservices20161110094537.azurewebsites.net/GuardAlertsResolve")
            
            let request = NSMutableURLRequest(URL: url!)
            
            request.HTTPMethod = "POST"
            
            request.setValue("Bearer \(appdelegate.accesstoken)", forHTTPHeaderField: "Authorization")
            
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            
            request.HTTPBody = jsonData
            
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request){ data, response, error in
                
                if error != nil{
                    print("Error -> \(error)")
                    return
                }
                else
                {
                    do
                    {
                        let resultforUserInfo = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? [String:AnyObject]
                        print(" response  is : \(resultforUserInfo)")
                        let httpResponse = response as? NSHTTPURLResponse
                        self.resolveStatusCode = httpResponse?.statusCode
                        
                        print("resolve status code \(self.statusCode1)")
                        if self.resolveStatusCode == 200
                        {
                            self.resolveBTN.hidden = true
                            self.displayAlertController("Resolve Alert", message: "Your alert has been resolved")
                            
                            
                        }
                        else
                        {
                             self.displayAlertController("Resolve Alert", message: "Problem with the service")
                            
                        }
                    }
                    catch
                    {
                        print("error")
                    }
                }
            }
            task.resume()
            //return tas
            
        } catch {
            print(error)
        }
        
   
    }
    
    /**
     This function displays the alerts with a message based on user action
     
     - Parameter  title: title of alert
     - Parameter  message: description of alert
     */
    func displayAlertController(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: {UIAlertAction -> Void in
            
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    /**
     This function will get the Claim request service
     - Parameter params: parameters for claim request service
     - Parameter url: url for claim request service
     */
    func serviceForClaimRequest(params:Dictionary<String,Int>,url:String){
        
        do{
            
            let jsonData = try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            
            let url = NSURL(string: "http://partyguardservices20161110094537.azurewebsites.net/GuardAlertsClaim")
            
            let request = NSMutableURLRequest(URL: url!)
            
            request.HTTPMethod = "POST"
            
            request.setValue("Bearer \(appdelegate.accesstoken)", forHTTPHeaderField: "Authorization")
            
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            
            request.HTTPBody = jsonData
            
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request){ data, response, error in
                
                if error != nil{
                    print("Error -> \(error)")
                    return
                }
                else
                {
                    do
                    {
                        let resultforUserInfo = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? [String:AnyObject]
                        print(" response  is : \(resultforUserInfo)")
                        let httpResponse = response as? NSHTTPURLResponse
                        self.statusCode1 = httpResponse?.statusCode
                        print("claim status code \(self.statusCode1)")
                        if self.statusCode1 == 200{
                            print("Claim success")
                            self.claimBTN.hidden = true
                            self.displayAlertController("Claim Alert", message: "Your Alert has been claimed")
                            
                        }
                        else{
                            print("Claim Unsuccess")
                        }
                    }
                    catch
                    {
                        print("error")
                    }
                }
            }
            task.resume()
            //return tas
            
        } catch {
            print(error)
        }

        
//        //print(params.debugDescription)
//        print("post method")
//        
//        let defaultConfigObject:NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
//        let session:NSURLSession = NSURLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: NSOperationQueue.mainQueue())
//        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
//        //request.setValue("Bearer \(appdelegate.accesstoken)", forHTTPHeaderField: "Authorization")
//        //session = NSURLSession.sharedSession()
//        request.HTTPMethod = "POST"
//        //var task
//        var err: NSError?
//        do
//        {
//            print("do loop")
//            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: [])
//            request.setValue("Bearer "+appdelegate.accesstoken, forHTTPHeaderField: "Authorization")
//            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//            request.addValue("application/json", forHTTPHeaderField: "Accept")
//            print("request is ALERTDETAILVC\(request)")
//            var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
//                print("Response ALERTDETAILVC : \(response)")
//                var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                print("Body ALERTDETAILVC : \(strData)")
//                let httpResponse = response as? NSHTTPURLResponse
//                print("response code  in ALERTDETAILVC \(httpResponse?.statusCode)")
//                self.statusCode1 = httpResponse?.statusCode
//                
//                dispatch_async(dispatch_get_main_queue()){
//                    NSNotificationCenter.defaultCenter().postNotificationName("Data Delivered", object: nil) // so as to execute on main thread
//                }
//                
//                
//            }).resume()
//            
//        }
//        catch
//        {
//            print("something is wrong")
//            //callbackforRegister()
//        }
//        
        
        
        

        
        
        
        
    }
    
    

}
